﻿ using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using System;
 using DataContext;
 using Entities;
 using Microsoft.EntityFrameworkCore;

 namespace DataService {
     public interface IHDepartmentService {
         Task<bool> CreateHospitalDepartment (string hospital, HDepartment department);
         Task<bool> UpdateHospitalDepartment (HDepartment department);
         Task<bool> DeleteHospitalDepartment (string hospital, string departmentId);
         Task<HDepartment> GetHDepartmentById (string departmentId, string hospitalId = "");
         Task<ICollection<HDepartment>> GetHDepartments (string hospital);

     }

     /// <summary>
     /// Dependent on the hospital service
     /// Should not be called directly, but should be used in the hospital service
     /// </summary>
     public class HDepartmentService : IHDepartmentService {
         // private IWardService _wardService;
         //private IHospital _hos;
         private IRepo<HDepartment> _repo;
         private IHospitalService _hosService;

         public HDepartmentService (IRepo<HDepartment> repo, IHospitalService hosService) {
             _hosService = hosService;
             _repo = repo;
         }

         //HDepartment Operations
         public async Task<bool> CreateHospitalDepartment (string hospitalId, HDepartment department) {
             try {
                 if (string.IsNullOrEmpty (hospitalId) ||
                     string.IsNullOrEmpty (department.Name) ||
                     string.IsNullOrEmpty (department.About) ||
                     string.IsNullOrEmpty (department.HOD) ||
                     !string.IsNullOrEmpty (department.HDepartmentId)) return false;

                 //Check if HDepartment exist
                 var hos = _repo.Context.FindAsync<Hospital> (hospitalId);

                 var existingDept = hos.Result.Departments.Where (x => x.Name.Equals (department.Name, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault ();

                 if (existingDept != null) return false; //HDepartment exist

                 //Add department to hospital
                 hos.Result.AddHDepartment (department);
                 await _repo.Context.AddAsync (department);

                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;

         }

         public async Task<bool> UpdateHospitalDepartment (HDepartment department) {
             try {
                 if (string.IsNullOrEmpty (department.Name) ||
                     string.IsNullOrEmpty (department.About) ||
                     string.IsNullOrEmpty (department.HOD) ||
                     string.IsNullOrEmpty (department.HDepartmentId) ||
                     string.IsNullOrEmpty (department.HospitalId)) return false;

                 var hos = await _repo.Context.FindAsync<Hospital> (department.HospitalId);
                 department.Hospital = hos as Hospital;
                 _repo.Context.Entry (department).State = EntityState.Detached;
                 _repo.Context.Update (department);
                 _repo.Context.Entry (hos).State = EntityState.Detached;
                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<bool> DeleteHospitalDepartment (string hospitalId, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (hospitalId) ||
                     string.IsNullOrEmpty (departmentId)) return false;

                 var hos = await _hosService.GetHospitalById (hospitalId);
                 if (hos == null) {
                     return false;
                 }

                 if (await GetHDepartmentById (departmentId, hospitalId) != null) {
                     return await _repo.Delete (departmentId);
                 }

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<HDepartment> GetHDepartmentById (string departmentId, string hospitalId = "") {
             try {
                 if (string.IsNullOrEmpty (departmentId)) return null;

                 //search the context directly
                 var department = (await _repo.Get (x => x.HDepartmentId == departmentId, null, "Wards,Labs,Staffs")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (hospitalId) && department.HospitalId != hospitalId) {
                     return null;
                 }
                 return department;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

         public async Task<ICollection<HDepartment>> GetHDepartments (string hospitalId) {
             try {
                 if (string.IsNullOrEmpty (hospitalId)) return null;
                 var hos = await _hosService.GetHospitalById (hospitalId);

                 if (hos != null) {
                     var deptList = await _repo.Get (d => string.Equals (d.HospitalId, hospitalId, StringComparison.InvariantCultureIgnoreCase), null, "");
                     return deptList.ToList ();
                 }
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

     }

 }