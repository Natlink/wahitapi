﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;
// using System.Text;
// using System.Threading.Tasks;
// using Entities;
// using Ninject;

// namespace DataService
// {
//     public class TestData
//     {
//         private IHospitalService _hospitalService;
//         private IPatientService _patientService;
//         private List<IHospital> _hospitals;
//         private IHospital _hospital;
//         private List<IPerson> _patients;
//         private IKernel _kernel;

//         public TestData()
//         {
//             _kernel = new StandardKernel();
//             _kernel.Load(Assembly.GetExecutingAssembly());

//             _hospitals = new List<IHospital>();
//             _patients = new List<IPerson>();
//             _hospitalService = _kernel.Get<IHospitalService>();
//             _patientService = _kernel.Get<IPatientService>();

//             _patientService.Repository = _hospitalService.Repository;

//         }

//         public async Task<bool> Load()
//         {
//             //Check if data is already loaded.
//             if (_hospitalService.GetHospital("Wahit Foundation Hospital") == null)
//             {
//                 await Task.Run(() => CreatHospital());
//             }
//             else
//             {
//                 CreatePatients();
//                 CreatePatientRecords();
//                 AddToQueue();
//             }
          
//             return true;
//         }

//         private async Task<bool> CreatHospital()
//         {
//             //Create  2 hospitals  for testing with registered staff
//             /***********************Wahit Foundation Hospital************/
//             var WFH = new Hospital
//             {
//                 Name = "Wahit Foundation Hospital"
//             };
//             WFH.Address = new HospitalAddress
//             {
//                 Country = "Ghana",
//                 State = "AS",
//                 Street = "Asante Kwame's Street",
//                 Town = "Kumasi",
//                 MapCordinateLatitude = "-0.55787",
//                 MapCordinateLongitude = "6.23876"
//             };

//             WFH.About = "I dont have anything good about your hospital";
//             WFH.WorkingHours = "24/7";

//             //Add hospital to database
//             try
//             {
//                 await Task.Run(() => _hospitalService.AddOrUpdateHospital(WFH));
//                 _hospital = _hospitalService.GetHospital("Wahit Foundation Hospital");

//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

//             //Wahit Foundation Hospital HDepartments
//             var WFHDepts = new List<HDepartment>
//             {
//                new HDepartment
//             {
                
//                 HOD = "Kwame Wahit",
//                 Name = "Maternity",
//                 AssistantHOD = "Kofi Wahit",
//             },
//                new HDepartment
//             {
//                 HOD = "John Wahit",
//                 Name = "Gastroenterology",
//                 AssistantHOD = "Goodluck Wahit",
//             },
//                new HDepartment
//                {
//                     HOD = "Bridget Wahit",
//                 Name = "Anaesthetics",
//                 AssistantHOD = "Andrew Wahit",
//                }
//             };

//             //Add HDepartments using HDepartment service
//             try
//             {
//                 WFHDepts.ForEach(dpt => _hospital.AddHDepartment(dpt));
//                 _hospitalService.AddOrUpdateHospital(_hospital);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

            

//             //Labs for WFH
//             var WFHLabs = new List<Lab>
//             {
//               new Lab
//             {
//                 Name = "Optical lab",

//             },
//               new Lab
//             {
//                 Name = "Blood and Unrine Culture lab",
//             },
//               new Lab
//             {
//                 Name = "Diagnostic lab",
//             }
//             };

//             //Add labs to the service
//             try
//             {
//                 WFHLabs.ForEach(lab=> _hospital.AddLab(lab));
//                 _hospitalService.AddOrUpdateHospital(_hospital);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

//             var WFHWard = new List<Ward>
//             {
//                 new Ward
//                 {
//                     Name = "Jerry Tom's ward",
//                     Type = WardType.Male,
//                 },
//                 new Ward
//                 {
//                     Name = "Public Ward",
//                     Type = WardType.Female,
//                 }
//             };

//             try
//             {
//                 var dept = _hospitalService.HDepartmentService.GetHDepartment(_hospital.HospitalId,"Maternity");
//                 WFHWard.ForEach(ward => dept.AddWard(ward));
//                 _hospitalService.AddOrUpdateHospital(_hospital);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

//             #region //Creating jerry toms ward componets
//             var WFHJeryTomsWardRooms = new List<WardRoom>
//             {
//                 new WardRoom
//             {
//                 Number = "JT34334",
//             },
//                 new WardRoom
//             {
//                 Number = "JT34335",
//             }

//            };

           
//             var WFHJeryTomsWardRooms1Beds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 }
//             };
//             var WFHJeryTomsWardRooms2Beds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 }
//             };

//             try
//             {
//                 var dept = _hospitalService.HDepartmentService.GetHDepartment(_hospital.HospitalId, "Maternity");
//                 var ward = _hospitalService.HDepartmentService.WardService.GetWard(_hospital.HospitalId, dept.HDepartmentId, "Jerry Tom's ward");
//                 foreach (var room in WFHJeryTomsWardRooms)
//                 {
//                     ward.AddRoom(room);
//                 }
//                 _hospitalService.AddOrUpdateHospital(_hospital);

//                 //Add Beds to Jerry Tom wardrooms
//                 var wardRooms = _hospitalService.HDepartmentService.WardService.GetWard(_hospital.HospitalId, dept.HDepartmentId, ward.WardId).Rooms;
//                 if (wardRooms == null) return false;
//                 {
//                     for (var i = 0; i < wardRooms.Count; i++)
//                     {
//                         switch (i)
//                         {
//                             case 0:
//                                 WFHJeryTomsWardRooms1Beds.ForEach(
//                           bed => wardRooms.ToList()[i].AddBed(bed));

//                                 break;
//                             case 1:
//                                 WFHJeryTomsWardRooms2Beds.ForEach(
//                           bed => wardRooms.ToList()[i].AddBed(bed));

//                                 break;
//                         }

//                     }
//                     _hospitalService.AddOrUpdateHospital(_hospital);
//                 }


//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

            
//             #endregion


//             #region //Creating Public ward componets
//             var WFHPublicWardRooms = new List<WardRoom>
//             {
//                 new WardRoom
//             {
//                 Number = "PB34334",
//             },new WardRoom
//             {
//                 Number = "PB34335",
//             }
//            };
           
//             var WFHPublicWardRooms1Beds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 },new Bed
//                 {
//                     Location = "3,1",
//                     Number = "5"
//                 },new Bed
//                 {
//                     Location = "3,2",
//                     Number = "6"
//                 }
//             };
//             var WFHPublicWardRooms2Beds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 },new Bed
//                 {
//                     Location = "3,1",
//                     Number = "5"
//                 },new Bed
//                 {
//                     Location = "3,2",
//                     Number = "6"
//                 }
//             };

//             //Add Beds to Public ward rooms
//             try
//             {
//                 //Get Ward rooms and if there is none return;
//                 var dept = _hospitalService.HDepartmentService.GetHDepartment(_hospital.HospitalId, "Gastroenterology");
//                 //Add wards to the HDepartment
//                 WFHWard.ForEach(wd => dept.AddWard(wd));
//                 _hospitalService.AddOrUpdateHospital(_hospital);

//                 var ward = _hospitalService.HDepartmentService.WardService.GetWard(_hospital.HospitalId, dept.HDepartmentId, "Public Ward");
//                 //Add rooms
//                 foreach (var room in WFHPublicWardRooms)
//                 {
//                     ward.AddRoom(room);
//                 }
//                 _hospitalService.AddOrUpdateHospital(_hospital);


//                 var wardRooms = _hospitalService.HDepartmentService.WardService.GetWardRooms(_hospital.HospitalId,dept.HDepartmentId, ward.WardId);

//                 if (wardRooms == null) return false;
//                 {
//                     for (var i = 0; i < wardRooms.Count; i++)
//                     {
//                         switch (i)
//                         {
//                             case 0:
//                                 WFHPublicWardRooms1Beds.ForEach(
//                           bed => wardRooms.ToList()[i].AddBed(bed));
//                                 break;
//                             case 1:
//                                 WFHPublicWardRooms2Beds.ForEach(
//                           bed => wardRooms.ToList()[i].AddBed(bed));
//                                 break;
//                         }
                        
//                     }
//                 }
//                 _hospitalService.AddOrUpdateHospital(_hospital);

//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             #endregion


//             //Creating Hospital Staffs
//             var WFHstaffs = new List<IHospitalStaff>
//             {new Doctor
//                 {
//                     FirstName = "Samuel",
//                     LastName = "Kelly",
//                     MiddleName = "Junior",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Male,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "AABB",
//                     BloodGroup = "AB",
//                     BiometricScan = "dapfkn#@RRF@$#f",
//                     Contact = new Contact
//                     {
//                         City = "Accrah",
//                         Country = "Ghana",
//                         HouseAddress = "910 Clifford Place",
//                         State = "North Kaneshie",
//                         Email = "oneillbooker@bovis.com",
//                         HomePhone = "+(233) 523 47 1042",
//                         WorkPhone = "+(233) 223 21 1123",
//                         MobilePhone = "+(233) 512 27 2134",
//                         PostalCode = " P.O BOX 6141"
//                     },
//                     Shift = "Night",
//                     WorkingHours = "2:00pm to 1:00 am",
//                     Type = "Doctor",
//                     Specialty = "Gynaecology",
//                     HDepartment = _hospitalService.HDepartmentService.GetHDepartment(_hospital.HospitalId, "Maternity") as HDepartment
//                 },new LabTechnician()
//                 {
//                     FirstName = "Adrews",
//                     LastName = "Kwaku",
//                     MiddleName = "Wahit",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Male,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "AABO",
//                     BloodGroup = "O",
//                     BiometricScan = "))(*(N9U9B(UJH^%%&",
//                     Contact = new Contact
//                     {
//                         City = "Tarkwa",
//                         Country = "Ghana",
//                         HouseAddress = "910 Clifford Place",
//                         State = "North Kaneshie",
//                         Email = "kopito@bovis.com",
//                         HomePhone = "+(233) 445 47 1042",
//                         WorkPhone = "+(233) 455 21 1123",
//                         MobilePhone = "+(233) 445 27 2134",
//                         PostalCode = "P.O BOX 6141"
//                     },
//                     Shift = "Morning",
//                     WorkingHours = "6:00am - 11:00pm",
//                     Type = "LabTechnicien",
//                     Specialty = "DI give a shit",
//                     Lab = _hospitalService.GetLab(_hospitalService.GetHospital("Wahit Foundation Hospital").HospitalId,"Optical lab")
//                 },
//                 new Nurse()
//                 {
//                     FirstName = "Bridget",
//                     LastName = "Chioma",
//                     MiddleName = "Wahit",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Female,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "ABBO",
//                     BloodGroup = "B+",
//                     BiometricScan = "))456789(UJH^%%&",
//                     Contact = new Contact
//                     {
//                         City = "Lagos",
//                         Country = "Nigeria",
//                         HouseAddress = "910 Clifford Place",
//                         State = "River Sate",
//                         Email = "mybridget@wahit.com",
//                         HomePhone = "+(234) 445 47 1042",
//                         WorkPhone = "+(234) 455 21 1123",
//                         MobilePhone = "+(234) 445 27 2134",
//                         PostalCode = "P.O BOX 757"
//                     },
//                     Shift = "Afternoon",
//                     WorkingHours = "12:00am - 6:00pm",
//                     Type = "Nurse",
//                     Specialty = "Hearts surgery assistant"
//                 }

//             };

//             //ADD Staffs to the various HDepartment
//             //For example sake, these staff will be available in all the
//             //HDepartment
//             //USE STAFF SERVICE

//             try
//             {
//                 var depts = _hospital.HDepartments.ToList();
//                 depts.ForEach(dpt =>
//                  {
//                      WFHstaffs.ForEach(staff =>
//                      {
//                          _hospitalService.HospitalStaffService.AddOrUpdateStaff(_hospital.HospitalId, dpt.HDepartmentId, staff);
//                      });
//                  });

//                 //_hospitalService.AddOrUpdateHospital(_hospital);

//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }


//             #region          /***********************DONORS TRUST CLINIC************/
//             /*
//             var DTC = new Hospital
//             {
//                 Name = "DONORS TRUST CLINIC"
//             };
//             DTC.Address = new HospitalAddress
//             {
//                 Country = "Ghana",
//                 State = "AS",
//                 Street = "Asante Kwame's Street",
//                 Town = "Kumasi",
//                 MapCordinateLatitude = "-0.55787",
//                 MapCordinateLongitude = "6.23876"
//             };

//             DTC.About = "I dont have anything good about your hospital";
//             DTC.WorkingHours = "24/7";


//             //Wahit Foundation Hospital HDepartments
//             var DTCDepts = new List<HDepartment>
//             {
//                new HDepartment
//             {
//                 HOD = "Kwame Wahit",
//                 Name = "Maternity ",
//                 AssistantHOD = "Kofi Wahit",
//             },
//                new HDepartment
//             {
//                 HOD = "John Wahit",
//                 Name = "Gastroenterology",
//                 AssistantHOD = "Goodluck Wahit",
//             },
//                new HDepartment
//                {
//                     HOD = "Bridget Wahit",
//                 Name = "Anaesthetics",
//                 AssistantHOD = "Andrew Wahit",
//                }
//             };

//             //Labs for DTC
//             var DTCLabs = new List<Lab>
//             {
//               new Lab
//             {
//                 Name = "Optical lab",

//             },
//               new Lab
//             {
//                 Name = "Blood and Unrine Culture lab",
//             },
//               new Lab
//             {
//                 Name = "Diagnostic lab",
//             }
//             };

//             var DTCWard = new List<Ward>
//             {
//                 new Ward
//                 {
//                     WardName = "Jerry Tom's ward",
//                     Type = WardType.Male,
//                 },
//                 new Ward
//                 {
//                     WardName = "Public Ward",
//                     Type = WardType.Female,
//                 }
//             };

//             #region //Creating jerry toms ward componets
//             var DTCJeryTomsWardRooms = new List<WardRoom>
//             {
//                 new WardRoom
//             {
//                 Number = "JT34334",
//             },new WardRoom
//             {
//                 Number = "JT34335",
//             }
//            };

//             var DTCJeryTomsWardRoomsBeds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 },new Bed
//                 {
//                     Location = "3,1",
//                     Number = "5"
//                 }
//             };

//             //Add Beds to Jerry Tom wardrooms
//             DTCJeryTomsWardRoomsBeds.ForEach(bed =>
//             {
//                 DTCJeryTomsWardRooms.ForEach(room =>
//                 {
//                     room.AddBed(bed);
//                 });
//             });
//             //Add Rooms to Jerrys Ward;
//             DTCJeryTomsWardRooms.ForEach(room =>
//             {
//                 DTCWard[0].AddRoom(room);
//             });


//             #endregion


//             #region //Creating Public ward componets
//             var DTCPublicWardRooms = new List<WardRoom>
//             {
//                 new WardRoom
//             {
//                 Number = "PB34334",
//             },new WardRoom
//             {
//                 Number = "PB34335",
//             }
//            };

//             var DTCPublicWardRoomsBeds = new List<Bed>
//             {
//                 new Bed
//                 {
//                     Location = "1,1",
//                     Number = "1"
//                 },new Bed
//                 {
//                     Location = "1,2",
//                     Number = "2"
//                 },new Bed
//                 {
//                     Location = "2,1",
//                     Number = "3"
//                 },new Bed
//                 {
//                     Location = "2,2",
//                     Number = "4"
//                 },new Bed
//                 {
//                     Location = "3,1",
//                     Number = "5"
//                 },new Bed
//                 {
//                     Location = "3,2",
//                     Number = "6"
//                 }
//             };

//             //Add Beds to Jerry Tom wardrooms
//             DTCPublicWardRoomsBeds.ForEach(bed =>
//             {
//                 DTCPublicWardRooms.ForEach(room =>
//                 {
//                     room.AddBed(bed);
//                 });
//             });
//             //Add Rooms to Jerrys Ward;
//             DTCPublicWardRooms.ForEach(room =>
//             {
//                 //Public ward is 1 in the ward list
//                 DTCWard[1].AddRoom(room);
//             });
//             #endregion

//             //Add Wards, Labs, and HDepartment to the DTC hospitals
//             DTCDepts.ForEach(dept => { DTC.AddHDepartment(dept); });
//             DTCLabs.ForEach(lab => { DTC.AddLab(lab); });
//             DTCWard.ForEach(ward => { DTC.AddWard(ward); });

//             //Creating Hospital Staffs
//             var DTCstaffs = new List<HospitalStaff>
//             {
//                 new Doctor
//                 {
//                     FirstName = "Samuel",
//                     LastName = "Kelly",
//                     MiddleName = "Junior",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Male,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "AABB",
//                     BloodGroup = "AB",
//                     BiometricScan = "dapfkn#@RRF@$#f",
//                     Contact = new Contact
//                     {
//                         City = "Accra",
//                         Country = "Ghana",
//                         HouseAddress = "910 Clifford Place",
//                         State = "North Kaneshie",
//                         Email = "oneillbooker@bovis.com",
//                         HomePhone = "+(233) 523 47 1042",
//                         WorkPhone = "+(233) 223 21 1123",
//                         MobilePhone = "+(233) 512 27 2134",
//                         PostalCode = " P.O BOX 6141"
//                     },
//                     Shift = "Night",
//                     WorkingHours = "2:00pm to 1:00 am",
//                     Type = "Doctor",
//                     Specialty = "Gynaecology"
//                 },new LabTechnician()
//                 {
//                     FirstName = "Adrews",
//                     LastName = "Kwaku",
//                     MiddleName = "Wahit",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Male,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "AABO",
//                     BloodGroup = "O",
//                     BiometricScan = "))(*(N9U9B(UJH^%%&",
//                     Contact = new Contact
//                     {
//                         City = "Tarkwa",
//                         Country = "Ghana",
//                         HouseAddress = "910 Clifford Place",
//                         State = "North Kaneshie",
//                         Email = "kopito@bovis.com",
//                         HomePhone = "+(233) 445 47 1042",
//                         WorkPhone = "+(233) 455 21 1123",
//                         MobilePhone = "+(233) 445 27 2134",
//                         PostalCode = "P.O BOX 6141"
//                     },
//                     Shift = "Morning",
//                     WorkingHours = "6:00am - 11:00pm",
//                     Type = "LabTechnicien",
//                     Specialty = "Don't give a shit",
//                     Lab = _hospitalService.GetLabByName(_hospitalService.GetHospitalByName("DONORS TRUST CLINIC").Id,"Diagnostic lab")

//                 },
//                 new Nurse()
//                 {
//                     FirstName = "Bridget",
//                     LastName = "Chioma",
//                     MiddleName = "Wahit",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Female,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "ABBO",
//                     BloodGroup = "B+",
//                     BiometricScan = "))456789(UJH^%%&",
//                     Contact = new Contact
//                     {
//                         City = "Lagos",
//                         Country = "Nigeria",
//                         HouseAddress = "910 Clifford Place",
//                         State = "River Sate",
//                         Email = "mybridget@wahit.com",
//                         HomePhone = "+(234) 445 47 1042",
//                         WorkPhone = "+(234) 455 21 1123",
//                         MobilePhone = "+(234) 445 27 2134",
//                         PostalCode = "P.O BOX 757"
//                     },
//                     Shift = "Afternoon",
//                     WorkingHours = "12:00am - 6:00pm",
//                     Type = "Nurse",
//                     Specialty = "Heart surgery assistant"
//                 }

//             };

//             //ADD Staffs to the various HDepartment
//             //For example sake, these staff will be available in all the 
//             //HDepartment
//             DTCstaffs.ForEach(staff =>
//             {
//                 DTCDepts.ForEach(dept => dept.AddStaff(staff));
//             });


//             //ADD THE HOSPITALS TO DATABASE
//             _hospitals.Add(WFH);
//             _hospitals.Add(DTC);

//             try
//             {
//                 foreach (var hos in _hospitals)
//                 {
//                   await Task.Run(()=> _hospitalService.AddOrUpdate(hos));

//                 }
               
              
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             */
//             #endregion;
//             return true;
//         }


       

//         #region      //***************************PATIENTS************************

//         private  bool CreatePatients()
//         {

//             var WFHPatients = new List<Patient>
//             {
//                 new Patient
//                 {
//                     DateAdmitted = DateTime.Now.ToString(),
//                     DateDischarged = new DateTime(2015, 4, 30).ToString(),
//                     FirstName = "Samuel",
//                     LastName = "Kelly",
//                     MiddleName = "Junior",
//                     DateOfBirth = DateTime.Now.ToShortDateString(),
//                     Gender = Gender.Male,
//                     MaritalStatus = MaritalStatus.Single,
//                     Genotype = "AOBO",
//                     BloodGroup = "AB",
//                     BiometricScan = "dapfkn#@RR@#$F@$jkj#f",
//                     Contact = new Contact
//                     {
//                         City = "Accra",
//                         Country = "Ghana",
//                         HouseAddress = "910 Clifford Place",
//                         State = "North Kaneshie",
//                         Email = "oneillbookero@bovis.com",
//                         HomePhone = "+(233) 523 47 1042",
//                         WorkPhone = "+(233) 223 21 1123",
//                         MobilePhone = "+(233) 512 27 2134",
//                         PostalCode = " P.O BOX 6141"
//                     },Folder = new Folder()
//                 },
//                 new Patient
//                 {
//                     DateAdmitted = DateTime.Now.ToString(),
//                     DateDischarged = new DateTime(2015, 4, 30).ToString(),
//                     FirstName = "Esther",
//                     LastName = "Watts",
//                     MiddleName = "",
//                     DateOfBirth = DateTime.Now.ToString(),
//                     Gender = Gender.Female,
//                     MaritalStatus = MaritalStatus.Married,
//                     Genotype = "BB",
//                     BloodGroup = "A+",
//                     BiometricScan = "dapfk123n#@RRF@$#fe",
//                     Contact = new Contact
//                     {
//                         City = "Oneida",
//                         Country = "Connecticut",
//                         HouseAddress = "293 Adler Place",
//                         State = "Pennsylvania",
//                         Email = "estherwatts1@bovis.com",
//                         HomePhone = "+233 (849) 479-3353",
//                         WorkPhone = "+233 (529) 423-9232",
//                         MobilePhone = "+233 (500) 339-0051",
//                         PostalCode = " P.O BOX 6670"
//                     },Folder = new Folder()
//                 },new Patient
//             {
//                 DateAdmitted = DateTime.Now.ToString(),
//                 DateDischarged = new DateTime(2011, 4, 30).ToString(),
//                 FirstName = "Samuel",
//                 LastName = "Yahoo",
//                 MiddleName = "Kellly",
//                 DateOfBirth = new DateTime(1999, 4, 2).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "AA",
//                 BloodGroup = "AB",
//                 BiometricScan = "daprtewfkn#@RRF@$#f1",
//                 Contact = new Contact
//                 {
//                     City = "Accra",
//                     Country = "Ghana",
//                     HouseAddress = "910 Clifford Place",
//                     State = "North Kaneshie",
//                     Email = "oneillgbEooker@bovis.com",
//                     HomePhone = "+(233) 523 47 1042",
//                     WorkPhone = "+(233) 223 21 1123",
//                     MobilePhone = "+(233) 512 27 2134",
//                     PostalCode = " P.O BOX 6141"
//                 },Folder = new Folder()
//             },
//                 new Patient
//             {
                
//                 DateAdmitted = new DateTime(2009, 1, 1).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
//                 FirstName = "Mcdonald",
//                 LastName = "York",
//                 MiddleName = "",
//                 DateOfBirth = new DateTime(1990, 9, 2).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Married,
//                 Genotype = "aaB",
//                 BloodGroup = "B",
//                 BiometricScan = "dapfoiuytkn#@RRF@$#f3",
//                 Contact = new Contact
//                 {
//                     City = "Oretta",
//                     Country = "Rhode Island",
//                     HouseAddress = "160 Lawn Court",
//                     State = "Maine",
//                     Email = "mckenziejimenez@bovis.com",
//                     HomePhone = "+233 (829) 495-3862",
//                     WorkPhone = "+233 (948) 485-2519",
//                     MobilePhone = "+233 (969) 550-2070",
//                     PostalCode = "P.O. Box 4216"
//                 },Folder = new Folder()
//             },
//                 new Patient
//             {
//                 DateAdmitted = new DateTime(2013, 8, 3).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
//                 FirstName = "Alexis",
//                 LastName = "Spence",
//                 MiddleName = "French",
//                 DateOfBirth = new DateTime(1998, 8, 24).ToString(),
//                 Gender = Gender.Female,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "AABO",
//                 BloodGroup = "AB",
//                 BiometricScan = "dapfkn#@RRF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "Harborton",
//                     Country = "RViet Nam",
//                     HouseAddress = "686 Channel Avenue",
//                     State = "New York",
//                     Email = "frenchsprtreence@bovis.com",
//                     HomePhone = "+233 (819) 543-2187",
//                     WorkPhone = "+233 (934) 496-3660",
//                     MobilePhone = "+233 (825) 451-2261",
//                     PostalCode = "P.O. Box  6002"
//                 },Folder = new Folder()
//             }
//             };

           
           


//             var DTCPatients = new List<Patient>
//             {
//                 new Patient
//             {
//                 DateAdmitted = new DateTime(2017, 2, 3).ToString(),
//                 DateDischarged = new DateTime(2017, 4, 4).ToString(),
//                 FirstName = "Janelle",
//                 LastName = "Gibbons",
//                 MiddleName = "",
//                 DateOfBirth = DateTime.Now.ToString(),
//                 Gender = Gender.Female,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "BBO",
//                 BloodGroup = "B+",
//                  BiometricScan = "dapf324324kn#@RRFsd@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "Colorado",
//                     Country = "Tonga",
//                     HouseAddress = "293 Adler Place",
//                     State = "Pennsylvania",
//                     Email = "janealle234@gmail.com",
//                     HomePhone = "+233 (883) 959-9843",
//                     WorkPhone = "+233 (850) 0784-0864",
//                     MobilePhone = "+233 (540) 068-6345",
//                     PostalCode = "PMB 70"
//                 },Folder = new Folder()
//             },new Patient
//             {
//                 DateAdmitted = new DateTime(2016, 03, 23).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
//                 FirstName = "Webb",
//                 LastName = "Leach",
//                 MiddleName = "",
//                 DateOfBirth = new DateTime(1985, 10, 22).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "AAO",
//                 BloodGroup = "O-",
//                 BiometricScan = "dapfk76543n#@RRsdsF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "San Marino",
//                     Country = "Fowlerville",
//                     HouseAddress = "599 Huntington Street",
//                     State = "Iowa",
//                     Email = "worldwiddedwebb@samplemail.com",
//                     HomePhone = "+233 (203) 537-2321",
//                     WorkPhone = "+233 (245) 524-2241",
//                     MobilePhone = "+223 (234) 524-3224",
//                     PostalCode = "P.O. Box 643-1"
//                 },Folder = new Folder()
//             },
//                 new Patient
//             {
//                 DateAdmitted = new DateTime(2016, 1, 23).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
//                 FirstName = "Jones",
//                 LastName = "Tanner",
//                 MiddleName = "",
//                 DateOfBirth = new DateTime(1995, 1, 22).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "AAO",
//                 BloodGroup = "O+",
//                 BiometricScan = "dapfteredfdkn#@RRF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "French Polynesia",
//                     Country = "Ironton",
//                     HouseAddress = "963 Ellery Street",
//                     State = "Puerto Rico",
//                     Email = "jonesdtanner@bovis.com",
//                     HomePhone = "+233 (969) 583-3046",
//                     WorkPhone = "+233 (934) 432-3236",
//                     MobilePhone = "+223 (433) 523-3009",
//                     PostalCode = "P.O. Box 2314"
//                 },Folder = new Folder()
//             },new Patient
//             {
//                 DateAdmitted = new DateTime(2017, 01, 03).ToString(),
//                 DateDischarged = new DateTime(2017, 04, 01).ToString(),
//                 FirstName = "Bowman",
//                 LastName = "Bell",
//                 MiddleName = "Kofi",
//                 DateOfBirth = new DateTime(1991, 5, 6).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "AABB",
//                 BloodGroup = "AB",
//                  BiometricScan = "dasdsdfspfkn#@RRF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "Elfrida",
//                     Country = "Solomon Islands",
//                     HouseAddress = "252 Halsey Street",
//                     State = "Illinois",
//                     Email = "bowmanbedfdhll@bovis.com",
//                     HomePhone = "+233 (898) 574-3827",
//                     WorkPhone = "+233 (835) 599-2298",
//                     MobilePhone = "+233 (836) 545-2289",
//                     PostalCode = "P.O. Box 1162"
//                 },Folder = new Folder()
//             },new Patient
//             {
                
//                 DateAdmitted = new DateTime(2009, 5, 7).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
               
//                 FirstName = "Mayra",
//                 LastName = "Dudley",
//                 MiddleName = "",
//                 DateOfBirth = new DateTime(1975, 1, 12).ToString(),
//                 Gender = Gender.Female,
//                 MaritalStatus = MaritalStatus.Married,
//                 Genotype = "BBO-",
//                 BloodGroup = "A+",
//                  BiometricScan = "dapgwefkn#@345RRF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "San Marino",
//                     Country = "Mauritania",
//                     HouseAddress = "599 Huntington Street",
//                     State = "Iowa",
//                     Email = "mayradud34ley@bovis.com",
//                     HomePhone = "+233 (963) 558-2459",
//                     WorkPhone = "+233 (945) 565-3706",
//                     MobilePhone = "+233 (920) 555-3873",
//                     PostalCode = "P.O. Box 378"
//                 },Folder = new Folder()
//             },new Patient
//             {
//                 DateAdmitted = new DateTime(2009, 1, 1).ToString(),
//                 DateDischarged = new DateTime(2016, 04, 01).ToString(),
//                 FirstName = "Mckenzie",
//                 LastName = "Jimenez",
//                 MiddleName = "Bond",
//                 DateOfBirth = new DateTime(1997, 2, 11).ToString(),
//                 Gender = Gender.Male,
//                 MaritalStatus = MaritalStatus.Single,
//                 Genotype = "BBO+",
//                 BloodGroup = "B",
//                  BiometricScan = "dapfknsdf#@RRF@$#f5",
//                 Contact = new Contact
//                 {
//                     City = "San Marino",
//                     Country = "Norfolk Island",
//                     HouseAddress = "599 Huntington Street",
//                     State = "Maine",
//                     Email = "mckenziejime98nez@bovis.com",
//                     HomePhone = "233 (887) 419-2955",
//                     WorkPhone = "+233 (896) 435-2722",
//                     MobilePhone = "+233 (988) 490-2228",
//                     PostalCode = "P.O. Box 8523"
//                 },Folder = new Folder()
//             }
//             };

//             try
//             {
//                 var hos = _hospitalService.GetHospital("Wahit Foundation Hospital");
//                 _patientService.Repository = _hospitalService.Repository;

//                 DTCPatients.ForEach(patient =>
//                 {
//                     _patientService.AddOrUpdatePatient(patient, hos);
//                 });

              
//                 return true;
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//                 throw (e);
//             }

            
//         }

//         bool CreatePatientRecords()
//         {
//             try
//             {
//                 //Get the patient
//                 //Get his folder
//                 var lab = _hospitalService.GetLab("Wahit Foundation Hospital", "Optical lab");
//                 var tech = _hospitalService.HospitalStaffService.GetStaff("Wahit Foundation Hospital", "Anaesthetics", "Adrews Kwaku Wahit");
//                var pat = _patientService.GetLastPatient() as Patient;
//                // pat.Folder.AddLabRecord(new LabData
//                // {
//                //    Lab = lab as Lab,
//                //    Note = "Eye problem",
//                //    DateTime = DateTime.Now.ToString(),
//                //    Technician = tech as LabTechnician,
//                //    TestName = "Retina Test",
//                //    TestResult = "Success with 80% accuracy",
                   
//                // });

//                 var surgRec = new SurgeryRecord
//                 {
//                     SurgeryDateTime = DateTime.Now.ToString(),
//                     Location = "Block 3, ",
//                     TheatreName = "Foundation Theatre 4",
//                     Surgeon =
//                         _hospitalService.HospitalStaffService.GetStaff("Wahit Foundation Hospital", "Anaesthetics", "Samuel Kelly") as Doctor

//                 };
                
//                 pat.Folder.AddSurgeryRecord(surgRec);
//                 pat.Folder.AddNote(new DoctorsNote
//                 { 
//                     DateRecorded = DateTime.Now.ToString(),
//                     DateUpdated = DateTime.Now.ToString(),
//                     Prescription = "You will need one bag of weed to survive",
//                     Doctor = _hospitalService.HospitalStaffService.GetStaff("Wahit Foundation Hospital", "Anaesthetics", "Samuel Kelly") as Doctor,


//                 });
//                 _patientService.AddOrUpdatePatient(pat, _hospitalService.GetHospital("Wahit Foundation Hospital"));
//                 var folderService = new FolderService();
//                 folderService.Patient = pat;
//                 folderService.Repository = _patientService.Repository;
//                 folderService.MapSurgeryRecordsToNurse(surgRec,
//                     _hospitalService.HospitalStaffService.GetStaff("Wahit Foundation Hospital", "Anaesthetics", "Bridget Chioma Wahit") as Nurse);
                
//             }
//             catch (Exception e) 
//             {
//                 Console.WriteLine(e.Message);   
//             }
//             return false;
//         }

//         void AddToQueue()
//         {
//             //var pat = _patientService.GetLastPatient() as Patient;
//             _patientService.AddPatientToQueue(new PatientQueue
//             {
//                 Doctor = _hospitalService.HospitalStaffService.GetStaff("Wahit Foundation Hospital", "Anaesthetics", "Samuel Kelly") as Doctor,
//                 Patient = _patientService.GetLastPatient() as Patient,
//                 PatientQueueId = "1"
//         });

//         }
//     }
// }
// #endregion