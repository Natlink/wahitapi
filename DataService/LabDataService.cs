 using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System;
 using DataContext;
 using Entities;
 using Microsoft.EntityFrameworkCore;

 namespace DataService {
     public interface ILabDataService {

         Task<bool> CreateLabData (LabData lab);
         Task<bool> DeleteLabData (int labDataId, int labId);
         Task<bool> UpdateLabData (LabData lab);
         Task<ILabData> GetLabDataById (int labDataId, int labId =0);
         Task<ICollection<LabData>> GetLabDataByName (string testName, int labId = 0);
         Task<ICollection<LabData>> GetAllLabData ();
         Task<ICollection<LabData>> GetLabDataByLab (int LabId);

     }

     /// <summary>
     /// Used to perform the various operation on labs
     /// Dependent on the Lab  service
     /// Should not be called directly, but should be used in the hospital service
     /// </summary>
     public class LabDataService : ILabDataService {

         public ILabService _labService;
         public IRepo<LabData> _repo;
         public LabDataService (IRepo<LabData> repo, ILabService labService) {
             _labService = labService;
             _repo = repo;

         }

         public async Task<bool> CreateLabData (LabData data) {
             try {
                 if (!data.IsValid()) return false;

                 //Check if lab exist
                 var lab = await _repo.Context.FindAsync<Lab> (data.LabId);

                 if (lab == null) return false;

                 // var existingLabData = GetLabDataByName (lab.Name, lab.LabId);
                 var existingLabData = await _repo.Context.Set<LabData> ().Where (x => x.Equals(data)).FirstOrDefaultAsync ();

                 if (existingLabData != null) return false; //Lab exist

                 //Add lab to lab
                 lab.AddLabData (data);
                 await _repo.Context.AddAsync (lab);

                 return await _repo.Context.SaveChangesAsync () >= 0;
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<bool> DeleteLabData (int labDataId, int labId=0) {
             try {
                 if (labId<0 ||
                     labDataId < 0) return false;

                 var lab = await _labService.GetLabById (labId);
                 if (lab == null) {
                     return false;
                 }

                 if (await GetLabDataById (labDataId, labId) != null) {
                     return await _repo.Delete (labDataId);
                 }

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<ILabData> GetLabDataById (int labDataId, int labId = 0) {
             try {
                 if (labDataId < 0) return null;

                 //search the context directly
                 var data = (await _repo.Get (x => x.LabDataId == labDataId, null, "")).FirstOrDefault ();

                 if (data.LabId != labId) {
                     return null;
                 }
                 return data;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;

         }

         public async Task<ICollection<LabData>> GetLabDataByName (string testName, int labId=0) {
             try {
                 if (string.IsNullOrEmpty (testName)) return null;

                 //search the context directly
                 var data = (await _repo.Get (x => string.Equals (x.TestName, testName, StringComparison.InvariantCultureIgnoreCase), null, "Rooms")).ToList();

                 return data;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

         public async Task<ICollection<LabData>> GetAllLabData () {
             try {
                 var labs = await _repo.Get (null, null, "");
                 if (labs != null)
                     return labs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;
         }

         public async Task<ICollection<LabData>> GetLabDataByLab (int labId) {
             try {
                 var labs = await _repo.Get (x => x.LabId == labId, null, "");
                 if (labs != null)
                     return labs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;

         }

         public async Task<bool> UpdateLabData (LabData data) {
             try {
                 if (!data.IsValid()) return false;

                 var lab = await _repo.Context.FindAsync<Lab> (data.LabId);
                 if (lab == null) return false;

                 data.Lab = lab;
                 _repo.Context.Update (lab);
                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 await _repo.Context.SaveChangesAsync ();
             }

             return false;
         }

     }
 }