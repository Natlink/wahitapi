using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataContext;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace DataService {
    public interface IWardRoomService {

        Task<bool> CreateWardRoom (WardRoom room);
        Task<bool> DeleteWardRoom (string roomId, string wardId);
        Task<bool> UpdateWardRoom (WardRoom room);
        Task<WardRoom> GetWardRoomById (string roomId, string wardId = "");
        Task<WardRoom> GetWardRoomByNumber (string roomName, string wardId = "");
        Task<ICollection<WardRoom>> GetAllWardRooms ();
        Task<ICollection<WardRoom>> GetWardRoomsByWard (string WardId);

        Task<bool> AddBed (Bed bed);
        Task<bool> DeleteBed (int bedId, string roomId);
        Task<bool> UpdateBed (Bed bed);
        Task<ICollection<Bed>> GetAllBeds ();
        Task<ICollection<Bed>> GetBedsByRoom (string roomId);
        Task<bool> AddPatientToBed (int bedId, string patientId);
        Task<bool> ClearBed (int bedId, string patientId);

    }

    /// <summary>
    /// Used to perform the various operation on rooms, room rooms and beds
    /// Dependent on the hospital service
    /// Should not be called directly, but should be used in the hospital service
    /// </summary>
    public class WardRoomService : IWardRoomService {

        public IWardService _wardService;
        public IRepo<WardRoom> _repo;
        public WardRoomService (IRepo<WardRoom> repo, IWardService wardService) {
            _wardService = wardService;
            _repo = repo;

        }

        public async Task<bool> CreateWardRoom (WardRoom room) {
            try {
                if (string.IsNullOrEmpty (room.WardId) ||
                    string.IsNullOrEmpty (room.WardRoomId) ||
                    string.IsNullOrEmpty (room.Number)) return false;

                //Check if ward exist
                var ward = await _repo.Context.FindAsync<Ward> (room.WardId);

                if (ward == null) return false;

                // var existingWardRoom = GetWardRoomByName (room.Name, room.WardId);
                var existingWardRoom = await _repo.Context.Set<WardRoom> ().Where (x => x.Number == room.Number).FirstOrDefaultAsync ();

                if (existingWardRoom != null) return false; //HWard exist

                //Add room to ward
                ward.AddRoom (room);
                await _repo.Context.AddAsync (room);

                return await _repo.Context.SaveChangesAsync () >= 0;
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return false;
        }

        public async Task<bool> DeleteWardRoom (string roomId, string wardId) {
            try {
                if (string.IsNullOrEmpty (wardId) ||
                    string.IsNullOrEmpty (roomId)) return false;

                var ward = await _wardService.GetWardById (wardId);
                if (ward == null) {
                    return false;
                }

                if (await GetWardRoomById (roomId, wardId) != null) {
                    return await _repo.Delete (roomId);
                }

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }

            return false;
        }

        public async Task<WardRoom> GetWardRoomById (string roomId, string wardId = "") {
            try {
                if (string.IsNullOrEmpty (roomId)) return null;

                //search the context directly
                var room = (await _repo.Get (x => x.WardRoomId == roomId, null, "Rooms")).FirstOrDefault ();

                if (!string.IsNullOrEmpty (wardId) && room.WardId != wardId) {
                    return null;
                }
                return room;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;

        }

        public async Task<WardRoom> GetWardRoomByNumber (string roomNumber, string wardId) {
            try {
                if (string.IsNullOrEmpty (roomNumber)) return null;

                //search the context directly
                var room = (await _repo.Get (x => string.Equals (x.Number, roomNumber, StringComparison.InvariantCultureIgnoreCase), null, "Beds")).FirstOrDefault ();

                if (!string.IsNullOrEmpty (wardId) && room.WardId != wardId) {
                    return null;
                }
                return room;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;
        }

        public async Task<ICollection<WardRoom>> GetAllWardRooms () {
            try {
                var rooms = await _repo.Get (null, null, "Rooms");
                if (rooms != null)
                    return rooms.ToList ();
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.WriteLine (e.StackTrace);
            }
            return null;
        }

        public async Task<ICollection<WardRoom>> GetWardRoomsByWard (string wardId) {
            try {
                var rooms = await _repo.Get (x => x.WardId == wardId, null, "Rooms");
                if (rooms != null)
                    return rooms.ToList ();
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.WriteLine (e.StackTrace);
            }
            return null;

        }

        public async Task<bool> UpdateWardRoom (WardRoom room) {
            try {
                if (!room.IsValid ()) return false;

                var ward = await _wardService.GetWardById (room.WardId);
                if (ward == null) return false;
                room.Ward = ward as Ward;

                _repo.Context.Entry (room).State = EntityState.Detached;
                _repo.Context.Update (room);
                _repo.Context.Entry (ward).State = EntityState.Detached;
                return await _repo.Context.SaveChangesAsync () >= 0;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }

            return false;
        }

        /*********************************************************
                SERVICES TO MANAGE BEDS IN ROOMS
         *********************************************************/
        public async Task<bool> AddBed (Bed bed) {
            try {
                if (!bed.IsValid ()) return false;

                //Check if Room exist
                var room = await _repo.Context.FindAsync<WardRoom> (bed.RoomId);

                if (room == null) return false;

                var existingBed = await _repo.Context.Set<Bed> ().Where (x => x.Number == bed.Number).FirstOrDefaultAsync ();

                if (existingBed != null) return false; //HWard exist

                //Add room to ward
                room.AddBed (bed);
                await _repo.Context.AddAsync (bed);

                return await _repo.Context.SaveChangesAsync () >= 0;
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return false;

        }

        public async Task<bool> DeleteBed (int bedId, string roomId) {
            try {
                if (bedId < 0) return false;

                var room = _repo.GetById (roomId);
                if (room == null || room.Result.Beds == null ||
                    !room.Result.Beds.Contains (new Bed { BedId = bedId })) {
                    return false;
                }

                _repo.Context.Remove (new Bed { BedId = bedId });
                return await _repo.Context.SaveChangesAsync () > 0;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }

            return false;
        }

        public async Task<bool> UpdateBed (Bed bed) {
            try {
                if (!bed.IsValid ()) return false;

                var room = await _repo.GetById (bed.RoomId);
                if (room == null) return false;

                bed.Room = room;

                _repo.Context.Entry (bed).State = EntityState.Detached;
                _repo.Context.Update (bed);
                _repo.Context.Entry (room).State = EntityState.Detached;
                return await _repo.Context.SaveChangesAsync () >= 0;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }

            return false;
        }

        public async Task<ICollection<Bed>> GetAllBeds () {
            try {
                var bed = _repo.Context.Set<Bed> ().ToListAsync ();

                if (bed != null)
                    return bed.Result;

            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.WriteLine (e.StackTrace);
            }
            return null;
        }

        public async Task<ICollection<Bed>> GetBedsByRoom (string roomId) {
            try {
                var beds = _repo.Context.Set<Bed> ().ToListAsync ();
                if (beds.Result == null) return null;

                return beds.Result.Where (x => string.Equals (x.RoomId, roomId)).ToList ();

            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.WriteLine (e.StackTrace);
            }
            return null;
        }

        public async Task<bool> AddPatientToBed (int bedId, string patientId) {
            try {
                //Find the bed 
                var bed = _repo.Context.FindAsync<Bed> (bedId);

                if (bed.Result == null) return false;

                //Find Person
                var patient = _repo.Context.FindAsync<Patient> (patientId);
                if (patient.Result == null) return false;

                bed.Result.AddPatient (patient.Result);

                _repo.Context.Entry (bed.Result).State = EntityState.Detached;
                _repo.Context.Update (bed.Result);
                _repo.Context.Entry (patient.Result).State = EntityState.Detached;
                return await _repo.Context.SaveChangesAsync () >= 0;

            } catch (Exception) {

            }
            return false;
        }

        public async Task<bool> ClearBed (int bedId, string patientId) {
            //Find the bed 
            var bed = _repo.Context.FindAsync<Bed> (bedId);

            if (bed.Result == null) return false;

            //Find Person
            var patient = _repo.Context.FindAsync<Patient> (patientId);
            if (patient.Result == null) return false;

            //Remove patient from bed
            bed.Result.ClearBed (patient.Result);

            _repo.Context.Entry (bed.Result).State = EntityState.Detached;
            _repo.Context.Update (bed.Result);
            _repo.Context.Entry (patient.Result).State = EntityState.Detached;
            return await _repo.Context.SaveChangesAsync () >= 0;

        }
    }
}