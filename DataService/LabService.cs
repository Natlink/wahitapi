﻿ using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System;
 using DataContext;
 using Entities;
 using Microsoft.EntityFrameworkCore;

 namespace DataService {
     public interface ILabService {

         Task<bool> CreateLab (Lab lab);
         Task<bool> DeleteLab (int labId, string departmentId);
         Task<bool> UpdateLab (Lab lab);
         Task<ILab> GetLabById (int labId, string departmentId = "");
         Task<ILab> GetLabByName (string labName, string departmentId = "");
         Task<ICollection<Lab>> GetAllLabs ();
         Task<ICollection<Lab>> GetLabsByDepartment (string DepartmentId);

     }

     /// <summary>
     /// Used to perform the various operation on labs
     /// Dependent on the Department  service
     /// Should not be called directly, but should be used in the hospital service
     /// </summary>
     public class LabService : ILabService {

         public IHDepartmentService _deptService;
         public IRepo<Lab> _repo;
         public LabService (IRepo<Lab> repo, IHDepartmentService deptService) {
             _deptService = deptService;
             _repo = repo;

         }

         public async Task<bool> CreateLab (Lab lab) {
             try {
                 if (string.IsNullOrEmpty (lab.DepartmentId) ||
                     string.IsNullOrEmpty (lab.Name) ||
                     lab.LabId < 0) return false;

                 //Check if department exist
                 var dept = await _repo.Context.FindAsync<HDepartment> (lab.DepartmentId);

                 if (dept == null) return false;

                 // var existingLab = GetLabByName (lab.Name, lab.DepartmentId);
                 var existingLab = await _repo.Context.Set<Lab> ().Where (x => x.Name == lab.Name).FirstOrDefaultAsync ();

                 if (existingLab != null) return false; //HDepartment exist

                 //Add lab to department
                 dept.AddLab (lab);
                 await _repo.Context.AddAsync (lab);

                 return await _repo.Context.SaveChangesAsync () >= 0;
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<bool> DeleteLab (int labId, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (departmentId) ||
                     labId < 0) return false;

                 var dept = await _deptService.GetHDepartmentById (departmentId);
                 if (dept == null) {
                     return false;
                 }

                 if (await GetLabById (labId, departmentId) != null) {
                     return await _repo.Delete (labId);
                 }

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<ILab> GetLabById (int labId, string departmentId = "") {
             try {
                 if (labId < 0) return null;

                 //search the context directly
                 var lab = (await _repo.Get (x => x.LabId == labId, null, "LabData,Technicians")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && lab.DepartmentId != departmentId) {
                     return null;
                 }
                 return lab;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;

         }

         public async Task<ILab> GetLabByName (string labName, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (labName)) return null;

                 //search the context directly
                 var lab = (await _repo.Get (x => string.Equals (x.Name, labName, StringComparison.InvariantCultureIgnoreCase), null, "Rooms")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && lab.DepartmentId != departmentId) {
                     return null;
                 }
                 return lab;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

         public async Task<ICollection<Lab>> GetAllLabs () {
             try {
                 var labs = await _repo.Get (null, null, "LabData,Technicians");
                 if (labs != null)
                     return labs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;
         }

         public async Task<ICollection<Lab>> GetLabsByDepartment (string departmentId) {
             try {
                 var labs = await _repo.Get (x => x.DepartmentId == departmentId, null, "LabData,Technicians");
                 if (labs != null)
                     return labs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;

         }

         public async Task<bool> UpdateLab (Lab lab) {
             try {
                 if (string.IsNullOrEmpty (lab.Name) ||
                     lab.LabId < 0 ||
                     string.IsNullOrEmpty (lab.DepartmentId)) return false;

                 var dept = await _repo.Context.FindAsync<HDepartment> (lab.DepartmentId);
                 if (dept == null) return false;
                 lab.Department = dept;

                 _repo.Context.Update (lab);
                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 await _repo.Context.SaveChangesAsync ();
             }

             return false;
         }

     }
 }