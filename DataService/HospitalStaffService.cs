﻿ using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System;
 using DataContext;
 using Entities;
 using Microsoft.EntityFrameworkCore;

 namespace DataService {
     public interface IHospitalStaffService {

         Task<bool> CreateHospitalStaff (HospitalStaff staff);
         Task<bool> DeleteHospitalStaff (string staffId, string departmentId);
         Task<bool> UpdateHospitalStaff (HospitalStaff staff);
         Task<IHospitalStaff> GetHospitalStaffById (string staffId, string departmentId = "");
         Task<IHospitalStaff> GetHospitalStaffByName (string staffName, string departmentId = "");
         Task<ICollection<HospitalStaff>> GetAllHospitalStaffs ();
         Task<ICollection<HospitalStaff>> GetHospitalStaffsByDepartment (string DepartmentId);

     }

     /// <summary>
     /// Used to perform the various operation on staffs
     /// Dependent on the Department  service
     /// Should not be called directly, but should be used in the hospital service
     /// </summary>
     public class HospitalStaffService : IHospitalStaffService {

         public IHDepartmentService _deptService;
         public IRepo<HospitalStaff> _repo;
         public HospitalStaffService (IRepo<HospitalStaff> repo, IHDepartmentService deptService) {
             _deptService = deptService;
             _repo = repo;

         }

         public async Task<bool> CreateHospitalStaff (HospitalStaff staff) {
             try {
                 if (!staff.IsValid ()) return false;

                 //Check if department exist
                 var dept = await _repo.Context.FindAsync<HDepartment> (staff.DepartmentId);

                 if (dept == null) return false;

                 var existingHospitalStaff = await _repo.Context.Set<HospitalStaff> ().Where (x => x.IsEqual (staff)).FirstOrDefaultAsync ();

                 if (existingHospitalStaff != null) return false; //HDepartment exist

                 //Add staff to department
                 dept.AddStaff (staff);
                 await _repo.Context.AddAsync (staff);

                 return await _repo.Context.SaveChangesAsync () >= 0;
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<bool> DeleteHospitalStaff (string staffId, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (departmentId) ||
                     string.IsNullOrEmpty (staffId)) return false;

                 var dept = await _deptService.GetHDepartmentById (departmentId);
                 if (dept == null) {
                     return false;
                 }

                 if (await GetHospitalStaffById (staffId, departmentId) != null) {
                     return await _repo.Delete (staffId);
                 }

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<IHospitalStaff> GetHospitalStaffById (string staffId, string departmentId = "") {
             try {
                 if (string.IsNullOrEmpty (staffId)) return null;

                 //search the context directly
                 var staff = (await _repo.Get (x => x.PersonId == staffId, null, "HospitalStaffData,Technicians")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && staff.DepartmentId != departmentId) {
                     return null;
                 }
                 return staff;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;

         }

         public async Task<IHospitalStaff> GetHospitalStaffByName (string staffName, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (staffName)) return null;

                 //search the context directly
                 var staff = (await _repo.Get (x => string.Equals (x.FullName, staffName, StringComparison.InvariantCultureIgnoreCase), null, "Rooms")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && staff.DepartmentId != departmentId) {
                     return null;
                 }
                 return staff;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

         public async Task<ICollection<HospitalStaff>> GetAllHospitalStaffs () {
             try {
                 var staffs = await _repo.Get (null, null, "");
                 if (staffs != null)
                     return staffs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;
         }

         public async Task<ICollection<HospitalStaff>> GetHospitalStaffsByDepartment (string departmentId) {
             try {
                 var staffs = await _repo.Get (x => x.DepartmentId == departmentId, null, "HospitalStaffData,Technicians");
                 if (staffs != null)
                     return staffs.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;

         }

         public async Task<bool> UpdateHospitalStaff (HospitalStaff staff) {
             try {
                 if (!staff.IsValid()||
                     string.IsNullOrEmpty (staff.DepartmentId)) return false;

                 var dept = await _repo.Context.FindAsync<HDepartment> (staff.DepartmentId);
                 if (dept == null) return false;
                 staff.Department = dept;

                 _repo.Context.Update (staff);
                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 await _repo.Context.SaveChangesAsync ();
             }

             return false;
         }

     }
 }