﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext;
using Entities;
using Microsoft.Extensions.Logging;

namespace DataService {
    public interface IHospitalService {
        //IHDepartmentService HDepartmentService { get; set; }
        //IHospitalStaffService HospitalStaffService { get; set; }

        Task<bool> AddHospital (IHospital hospital);
        Task<bool> UpdateHospital (IHospital hospital);
        Task<IHospital> GetHospitalByAddress (IHospitalAddress address);
        Task<bool> DeleteHospitalById (string id);
        Task<IHospital> GetHospitalById (string hospitalId);
        Task<IEnumerable<IHospital>> GetHospitals ();
        Task<string> GetPhotos (string hospitalId);

        // IEnumerable<IHospital> GetHospitals(int howMany, bool reset);

        // //Lab operations
        // ICollection<ILab> GetLabs(string hospital);
        // ILab GetLab(string hospital, string lab);

    }

    public class HospitalService : IHospitalService, IDisposable {
        private IRepo<Hospital> _repo;
        ILogger<HospitalService> _logger;

        public HospitalService (IRepo<Hospital> repo,ILogger<HospitalService> logger) {
            _repo = repo;
            _logger = logger;
           
        }

        public async Task<bool> AddHospital (IHospital hospital) {
            try {

                if (string.IsNullOrEmpty (hospital.Name)) {
                    throw (new ArgumentException ("Hospital contains invalid data"));
                }
                //Check if hospital exist with the same name at the same location
                IHospital existHospital = null;

                //Don't allow hospital without address to be saved
                 if (hospital.Address != null &&
                     !string.IsNullOrEmpty (hospital.Address.City) &&
                     !string.IsNullOrEmpty (hospital.Address.State) &&
                     !string.IsNullOrEmpty (hospital.Address.Country)) {
                     existHospital = await GetHospitalByAddress (new HospitalAddress {
                         Country = hospital.Address.Country,
                             State = hospital.Address.State,
                             City = hospital.Address.City
                     });
                 } else {
                     return false;
                 }

                //Hospital already exist in database
                if (existHospital != null) {
                    throw (new ArgumentException ("Hospital contains invalid data"));
                }
                
                return await _repo.Insert(hospital as Hospital);

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return false;
        }
        public async Task<bool> UpdateHospital (IHospital hospital) {
            try {
                //Update can only perform for object with Ids
                if (string.IsNullOrEmpty (hospital.HospitalId)) {
                    return false;
                }

                if (string.IsNullOrEmpty (hospital.Name) ||
                    string.IsNullOrEmpty (hospital.About) ||
                    hospital.Address == null) {
                    _logger.LogInformation ("Invalid Hospital data; not all required field are supplied");
                    throw (new ArgumentException ("Hospital does not have address, or some results fields"));
                }

                if (await GetHospitalById (hospital.HospitalId) == null) {
                    return false;
                }

                
                if( await _repo.Update (hospital as Hospital, hospital.HospitalId)){
                    _logger.LogInformation($"Hospital with ID: {hospital.HospitalId} have been update");
                    return true;
                }

            } catch (Exception e) {
                _logger.LogError (e,$"Error Occurred updating Hospital with ID: {hospital.HospitalId}");
                Console.WriteLine (e.Message);
            }
            return false;
            
        }

        public async Task<bool> DeleteHospitalById (string id) {
            try {
                if (string.IsNullOrEmpty (id))
                    return false;
                return await _repo.Delete (id);
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return false;
        }

        public async Task<IHospital> GetHospitalById (string hospitalId) {
            try {
                if (string.IsNullOrEmpty (hospitalId)) return null;
                 var hos = await _repo.Get(x=>x.HospitalId==hospitalId,null,"Address,Map,Departments");

                 return hos.FirstOrDefault();

            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;
        }

        public async Task<IEnumerable<IHospital>> GetHospitals () {
            try {
                return await _repo.Get (null,null,"Address,Map,Departments");

            } catch (Exception) {

            }
            return null;
        } 

        public async Task<IHospital> GetHospitalByAddress (IHospitalAddress address) {
            try {
                if (string.IsNullOrEmpty (address.City) ||
                    string.IsNullOrEmpty (address.Country) ||
                    string.IsNullOrEmpty (address.State)) return null;

                var hospitals = await _repo.Get (x =>
                    address.City.ToLower ().Contains (x.Address.City.ToLower ()) &&
                    address.State.ToLower ().Contains (x.Address.State.ToLower ()) &&
                    address.Country.ToLower ().Contains (x.Address.Country.ToLower ()));

                return hospitals.FirstOrDefault ();
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;
        }

        public async Task<IHospital> GetHospitalByMapCords (string lat, string lng) {
            try {
                if (string.IsNullOrEmpty(lat) ||
                    string.IsNullOrEmpty(lng)) {
                    return null;
                }

                var hospitals = await _repo.Get (x => lat==x.Map.lat &&
                    lng==x.Map.lng );

                return hospitals.FirstOrDefault ();
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;
        }

        public async Task<string> GetPhotos (string hospitalId) {
            try {
                if (string.IsNullOrEmpty (hospitalId)) return null;
                var hos = await GetHospitalById (hospitalId.Trim ());
                return hos.Photos;
            } catch (Exception e) {
                Console.WriteLine (e.Message);
            }
            return null;

        }

        public void Dispose () {
            _repo = null;

            GC.SuppressFinalize (this);
        }

        // //Lab Operations
        // public ILab GetLab(string hospital, string lab)
        // {
        //     try
        //     {
        //         return
        //             GetHospital(hospital.Trim())
        //                 .Labs.First(x => x.LabId.Equals(lab.Trim(), StringComparison.InvariantCultureIgnoreCase)
        //                                  || x.Name.Equals(lab.Trim(), StringComparison.InvariantCultureIgnoreCase));
        //     }
        //     catch (Exception e)
        //     {
        //         Console.WriteLine(e.Message);
        //         return null;
        //     }
        // }

        // public ICollection<ILab> GetLabs(string hospital)
        // {
        //     try
        //     {
        //         return GetHospital(hospital.Trim()).Labs;
        //     }
        //     catch (Exception e)
        //     {
        //         Console.WriteLine(e.Message);
        //         return null;
        //     }
        // }

    }

}