﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;
// using System.Threading.Tasks;
// using Entities;
// using DataLayer;
// using NHibernate.Util;
// using Ninject;

// namespace DataService
// {
//     public interface IPatientService
//     {
//         bool AddOrUpdatePatient(IPerson patient, IHospital hos);
//         bool DeletePatient(IPerson patient, IHospital hos);
//         IPerson GetLastPatient();
//         IPerson GetPatientById(string Id);
//         IPerson GetPatientByEmail(string email);
//         IEnumerable<IPerson> GetPatients(int howmany,bool reset);
//         IEnumerable<IPerson> GetPatientsByName(string firstname = null, string lastname = null, string middlename = null);
//         IEnumerable<IPerson> GetPatients();
//         IEnumerable<IPerson> GetPatientsByIllness(string illness);
//         INHRepo Repository { get; set; }
//         IFolderService FolderService { get; set; }
//         IEnumerable<PatientQueue> GetPatientQueue();
//         bool DeleteFromPatientQueue(string id);
//         bool AddPatientToQueue(PatientQueue patient);
//     }

//     public class PatientService : IPatientService
//     {
//         private INHRepo _repo;
//         private IPatientHospital _patHosMap;
//         private IFolderService _folderService;

//         private static int _nextBatch;

//         public PatientService(IPatientHospital phMap, INHRepo repository,IFolderService folderService)
//         {
            
//             _repo = repository;
//             _patHosMap = phMap;
//             _nextBatch = 0;

//             _folderService = folderService;
//             _folderService.Repository = _repo;
//         }

//         public INHRepo Repository
//         {
//             get { return _repo; }
//             set
//             {
//                 _repo = value;
//                 _folderService.Repository = _repo;
//             }
//         }

//         public IFolderService FolderService
//         {
//             get
//             {
//                 return _folderService;
//             }
//             set
//             {
//                 _folderService = value;
//                 _folderService.Repository = _repo;
//             }
//         }

//         public IPatientHospital PatientHospitalMap
//         {
//             get { return _patHosMap; }
//             set { _patHosMap = value; }
//         }

//         /// <summary>
//         /// Returns the last patient add to the system
//         /// </summary>
//         /// <returns>Patient</returns>
//         public IPerson GetLastPatient()
//         {
//             try
//             {
//                 return _repo.ToList<Patient>().ToList().LastOrDefault();
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }
//         public bool AddOrUpdatePatient(IPerson patient,IHospital hos)
//         {
//             try
//             {
//                 _patHosMap.Patient = patient as Patient;
//                 _patHosMap.Hospital = hos as Hospital;
            
//                 //_repo.CommitTransaction();

               
//                 //_repo.CommitTransaction();
//                 _repo.BeginTransaction();
//                 _repo.Save(hos);
//                 _repo.Save(patient);
//                 _repo.Save(_patHosMap);
//                 _repo.CommitTransaction();

//                 return true;
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//                 (_repo as NHRepositoryBase).RollbackTransaction();
//             }
//             return false;
//         }
//         public bool DeletePatient(IPerson patient, IHospital hos)
//         {
//             try
//             {
//                 var pathos =_repo.ToList<IPatientHospital>().FirstOrDefault(ph => ph.Patient.Id == patient.Id && ph.Hospital.HospitalId == hos.HospitalId);

//                 hos.PatientHospitals.Remove(pathos as PatientHospital);
//                 _repo.BeginTransaction();
//                 _repo.Save(hos);
//                 _repo.CommitTransaction();
//                 return true;
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//                 (_repo as NHRepositoryBase).RollbackTransaction();
//             }
//             return false;
//         }
//         public IPerson GetPatientById(string Id)
//         {
//             try
//             {
//                 return _repo.GetById(typeof(Patient), Id) as Patient;
//             }
//             catch (NullReferenceException e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }
//         public IPerson GetPatientByEmail(string email)
//         {
//             try
//             {
//                 return _repo.ToList<Patient>().FirstOrDefault(x => x.Contact.Email == email);
//             }
//             catch (NullReferenceException e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }
//         public IEnumerable<IPerson> GetPatients(int howmany,bool reset=true)
//         {
//             try
//             {
//                 //Starting reading from top
//                 if (reset) _nextBatch = 0;

//                 //if number patient is not specified, return all patient in database
//                 if (howmany == 0) return _repo.ToList<Patient>().ToList();

//                 var patients = _repo.ToList<Patient>().ToList().GetRange(_nextBatch, howmany - 1);//-1 is for zero correction
//                 _nextBatch = patients.Count();
//                 return patients;
//             }
//             catch (NullReferenceException e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }
//         /// <summary>
//         /// Return patients who have at least one of the name in the parameters
//         /// </summary>
//         /// <param name="Firstname"></param>
//         /// <param name="lastname"></param>
//         /// <param name="middlename"></param>
//         /// <returns></returns>
//         public IEnumerable<IPerson> GetPatientsByName(string firstname = null, string lastname = null, string middlename = null)
//         {
//             try
//             {
//                 return
//                     _repo.ToList<Patient>()
//                         .Where(
//                             patient =>
//                                 (string.Equals(patient.FirstName, firstname, StringComparison.InvariantCultureIgnoreCase) &&
//                                  string.Equals(patient.MiddleName, middlename,
//                                      StringComparison.InvariantCultureIgnoreCase) &&
//                                  string.Equals(patient.LastName, lastname, StringComparison.InvariantCultureIgnoreCase)
//                                     ));
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }

//             return null;
//         }
//         public IEnumerable<IPerson> GetPatients()
//         {
//             try
//             {
//                 return _repo.ToList<Patient>();
//             }
//             catch (NullReferenceException e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }
//         public IEnumerable<IPerson> GetPatientsByIllness(string illness)
//         {
//             throw new NotImplementedException();
//         }

//         public IEnumerable<PatientQueue> GetPatientQueue()
//         {
//             try
//             {
//                 return _repo.ToList<PatientQueue>().ToList();
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//             }
//             return null;
//         }

//         public bool DeleteFromPatientQueue(string patientId)
//         {
//             try
//             {
//                 var patient = _repo.ToList<PatientQueue>().FirstOrDefault(x => x.Patient.Id == patientId);//GetById(typeof (PatientQueue), Id);
//                 if (patient != null)
//                 {  
//                     _repo.BeginTransaction();
//                     _repo.Delete(patient);
//                     _repo.CommitTransaction();
//                     return true;
//                 }
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//                 (_repo as NHRepositoryBase).RollbackTransaction();
//             }
//             return false;
//         }

//         public bool AddPatientToQueue(PatientQueue patient)
//         {
//             try
//             {
//                 _repo.BeginTransaction();
//                 _repo.Save(patient);
//                 _repo.CommitTransaction();
//                 return true;
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.Message);
//                 (_repo as NHRepositoryBase).RollbackTransaction();
//             }
//             return false;
//         }
//     }


// }
