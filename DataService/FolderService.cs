﻿// using System;
// using System.Collections.Generic;
// using System.Linq;
// using Entities;
// using DataContext;

// namespace DataService
// {
//     public interface IFolderService
//     {
//         IFolder GetFolder();
//         IEnumerable<ILabData> GetLabRecords();
//         IEnumerable<ISurgeryRecord> GetSurgeryRecords();
//         IEnumerable<IDoctorsNote> GetNotes();
//         void DeleteNote<T>(T noteOrId);
//         void AddNote(IDoctorsNote note);
//         IEnumerable<IDoctorsNote> GetNotesByDate(string date);
//         IPatient Patient { get; set; }
//         bool MapSurgeryRecordsToNurse(ISurgeryRecord record, IPerson Nurse);
//         IRepo<Folder> Repository { get; set; }
//     }

//     public class FolderService : IFolderService
//     {
//         private IPatient _patient;
//         private IFolder _folder;
//         private IRepo<Folder> _repo;

//         public FolderService()
//         {
//         }
//         public IRepo<Folder> Repository
//         {
//             get { return _repo; }
//             set { _repo = value; }
//         }


//         public IFolder GetFolder()
//         {
//             return _folder;
//         }

//         public IPatient Patient
//         {
//             get { return _patient; }
//             set
//             {
//                 _patient = value;
//                 _folder = _patient.Folder;
//             }
//         }

//         /// <summary>
//         /// LAB RECORDS
//         /// </summary>
//         /// <returns></returns>
//         public IEnumerable<ILabData> GetLabRecords()
//         {

//             try
//             {
//                 return _folder.LabData;
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any lab records, null value for labrecords");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public void AddLabRecord(ILabData record)
//         {
//             _folder.AddLabData(record);
//         }

//         public void DeleteLabRecord<T>(string date, string testname)
//         {
//             try
//             {
//                 var record = _folder.LabData.FirstOrDefault(x => string.Equals(x.TestDateTime, date, StringComparison.InvariantCultureIgnoreCase) &&
//                string.Equals(x.TestName, testname, StringComparison.InvariantCultureIgnoreCase));

//                 if (record != null) _folder.LabData.Remove(record);

//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//         }

//         public IEnumerable<ILabData> GetLabRecordsByDate(string date)
//         {
//             try
//             {
//                 return _folder.LabData.Where(x => x.TestDateTime == date);
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public IEnumerable<ILabData> GetLabRecordsByTechnician(string technicianId)
//         {
//             try
//             {
//                 return _folder.LabData.Where(x => string.Equals(x.Technician.Id, technicianId, StringComparison.InvariantCultureIgnoreCase));
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }


//         /// <summary>
//         /// NOTES
//         /// </summary>
//         /// <returns></returns>
//         public IEnumerable<IDoctorsNote> GetNotes()
//         {
//             try { 
//             return _folder.Notes;
//         }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any Notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public void AddNote(IDoctorsNote note)
//         {
//             _folder.AddNote(note);
//         }

//         //Takes an object note or string id of a note
//         public void DeleteNote<T>(T noteOrId)
//         {
//             try
//             {
//                 IDoctorsNote _note;
//                 if (noteOrId.GetType() == typeof(IDoctorsNote) ||
//                     noteOrId.GetType() == typeof(DoctorsNote))
//                 {
//                     _note = _folder.Notes.FirstOrDefault(x => x.NoteId == (noteOrId as DoctorsNote).NoteId);

//                 }
//                 else
//                 {
//                     _note = _folder.Notes.FirstOrDefault(x => x.NoteId == (noteOrId as string));
//                 }
//                 if (_note != null) _folder.Notes.Remove(_note);

//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//         }

//         public IEnumerable<IDoctorsNote> GetNotesByDate(string date)
//         {
//             try
//             {
//                 return _folder.Notes.Where(x => x.DateRecorded == date);
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public IEnumerable<IDoctorsNote> GetNotesByCreator(string fname, string lname, string mname)
//         {
//             try
//             {
//                 return _folder.Notes.Where(x => string.Equals(x.Doctor.FirstName + " " + x.Doctor.LastName + " " + x.Doctor.MiddleName, fname + " " + lname + " " + mname, StringComparison.InvariantCultureIgnoreCase));
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }


//         /// <summary>
//         /// SURGERY RECORDS
//         /// </summary>
//         /// <returns></returns>
//         public IEnumerable<ISurgeryRecord> GetSurgeryRecords()
//         {
//             try {
//                 return _folder.SurgeryRecords;
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any surgery records, null value for surgery records");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public void AddSurgeryRecord(ISurgeryRecord record)
//         {

//             try
//             {
//                 _folder.AddSurgeryRecord(record);
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any surgery records, null value for surgery records");
//             }
//             catch (NullReferenceException)
//             {
//                 Console.WriteLine("User do not have any Folder, null value for folder object");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
            
//         }

//         //Takes an object note or string id of a note
//         public void DeleteSurgeryRecords<T>(T recordOrId)
//         {
//             try
//             {
//                 ISurgeryRecord record;
//                 if (recordOrId.GetType() == typeof(ISurgeryRecord) ||
//                     recordOrId.GetType() == typeof(SurgeryRecord))
//                 {
//                     record = _folder.SurgeryRecords.FirstOrDefault(x => x.SurgeryRecordId== (recordOrId as SurgeryRecord).SurgeryRecordId);

//                 }
//                 else
//                 {
//                     record = _folder.SurgeryRecords.FirstOrDefault(x => x.SurgeryRecordId == (recordOrId as string));
//                 }
//                 if (record != null) _folder.SurgeryRecords.Remove(record);

//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (NullReferenceException)
//             {
//                 Console.WriteLine("User do not have any Folder, null value for folder object");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//         }

//         public IEnumerable<ISurgeryRecord> GetSurgeryRecrodsByDate(string date)
//         {
//             try
//             {
//                 return _folder.SurgeryRecords.Where(x => x.SurgeryDateTime == date);
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any surgery records, null value for records");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public IEnumerable<ISurgeryRecord> GetSurgeryRecordsBySurgent(string fname, string lname, string mname)
//         {
//             try
//             {
//                 return _folder.SurgeryRecords.Where(x => string.Equals(x.Surgeon.FirstName + " " + x.Surgeon.LastName + " " + x.Surgeon.MiddleName, fname + " " + lname + " " + mname, StringComparison.InvariantCultureIgnoreCase));
//             }
//             catch (ArgumentException)
//             {
//                 Console.WriteLine("User do not have any notes, null value for notes");
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine("Unknown exception: ");
//                 Console.WriteLine(e.Message);
//                 Console.WriteLine("Inner Exception: ");
//                 if (e.InnerException != null)
//                 {
//                     Console.WriteLine(e.InnerException.Message);
//                 }
//             }
//             return null;
//         }

//         public bool MapSurgeryRecordsToNurse(ISurgeryRecord record, IPerson nurse)
//         {
//             try
//             {
//                 _repo.BeginTransaction();
//                 _repo.Save(new SurgeryRecordNurse(record as SurgeryRecord, nurse as Nurse));
//                 _repo.CommitTransaction();
//                 return true;
//             }
//             catch (Exception e)
//             {
//                 _repo.RollbackTransaction();
//                 _repo.CommitTransaction();
//             }
//             return false;
//         }
//     }
// }