﻿ using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System;
 using DataContext;
 using Entities;
 using Microsoft.EntityFrameworkCore;

 namespace DataService {
     public interface IWardService {

         Task<bool> CreateWard (Ward ward);
         Task<bool> DeleteWard (string wardId, string departmentId);
         Task<bool> UpdateWard (Ward ward);
         Task<IWard> GetWardById (string wardId, string departmentId = "");
         Task<IWard> GetWardByName (string wardName, string departmentId = "");
         Task<ICollection<Ward>> GetAllWards ();
         Task<ICollection<Ward>> GetWardsByDepartment (string DepartmentId);

     }

     /// <summary>
     /// Used to perform the various operation on wards, ward rooms and beds
     /// Dependent on the hospital service
     /// Should not be called directly, but should be used in the hospital service
     /// </summary>
     public class WardService : IWardService {

         public IHDepartmentService _deptService;
         public IRepo<Ward> _repo;
         public WardService (IRepo<Ward> repo, IHDepartmentService deptService) {
             _deptService = deptService;
             _repo = repo;

         }

         public async Task<bool> CreateWard (Ward ward) {
             try {
                 if (string.IsNullOrEmpty (ward.DepartmentId) ||
                     string.IsNullOrEmpty (ward.Type.ToString ()) ||
                     string.IsNullOrEmpty (ward.Name) ||
                     !string.IsNullOrEmpty (ward.WardId)) return false;

                 //Check if department exist
                 var dept = await _repo.Context.FindAsync<HDepartment>(ward.DepartmentId);

                 if (dept == null) return false;

                // var existingWard = GetWardByName (ward.Name, ward.DepartmentId);
                 var existingWard = await _repo.Context.Set<Ward> ().Where(x =>x.Name == ward.Name).FirstOrDefaultAsync();

                 if (existingWard != null) return false; //HDepartment exist

                 //Add ward to department
                 dept.AddWard(ward);
                 await _repo.Context.AddAsync (ward);

                 return await _repo.Context.SaveChangesAsync () >= 0;
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<bool> DeleteWard (string wardId, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (departmentId) ||
                     string.IsNullOrEmpty (wardId)) return false;

                 var dept = await _deptService.GetHDepartmentById (departmentId);
                 if (dept == null) {
                     return false;
                 }

                 if (await GetWardById (wardId, departmentId) != null) {
                     return await _repo.Delete (wardId);
                 }

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

         public async Task<IWard> GetWardById (string wardId, string departmentId = "") {
             try {
                 if (string.IsNullOrEmpty (wardId)) return null;

                 //search the context directly
                 var ward = (await _repo.Get (x => x.WardId == wardId, null, "Rooms")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && ward.DepartmentId != departmentId) {
                     return null;
                 }
                 return ward;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;

         }

         public async Task<IWard> GetWardByName (string wardName, string departmentId) {
             try {
                 if (string.IsNullOrEmpty (wardName)) return null;

                 //search the context directly
                 var ward = (await _repo.Get (x => string.Equals (x.Name, wardName, StringComparison.InvariantCultureIgnoreCase), null, "Rooms")).FirstOrDefault ();

                 if (!string.IsNullOrEmpty (departmentId) && ward.DepartmentId != departmentId) {
                     return null;
                 }
                 return ward;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }
             return null;
         }

         public async Task<ICollection<Ward>> GetAllWards () {
             try {
                 var wards = await _repo.Get (null, null, "Rooms");
                 if (wards != null)
                     return wards.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;
         }

         public async Task<ICollection<Ward>> GetWardsByDepartment (string departmentId) {
             try {
                 var wards = await _repo.Get (x => x.DepartmentId == departmentId, null, "Rooms");
                 if (wards != null)
                     return wards.ToList ();
             } catch (Exception e) {
                 Console.WriteLine (e.Message);
                 Console.WriteLine (e.StackTrace);
             }
             return null;

         }

         public async Task<bool> UpdateWard (Ward ward) {
             try {
                 if (string.IsNullOrEmpty (ward.Name) ||
                     string.IsNullOrEmpty (ward.Type.ToString ()) ||
                     string.IsNullOrEmpty (ward.WardId) ||
                     string.IsNullOrEmpty (ward.DepartmentId)) return false;

                 var dept = await _deptService.GetHDepartmentById (ward.DepartmentId);
                 if (dept == null) return false;
                 ward.Department = dept;

                 _repo.Context.Entry (ward).State = EntityState.Detached;
                 _repo.Context.Update (ward);
                 _repo.Context.Entry (dept).State = EntityState.Detached;
                 return await _repo.Context.SaveChangesAsync () >= 0;

             } catch (Exception e) {
                 Console.WriteLine (e.Message);
             }

             return false;
         }

     }
 }