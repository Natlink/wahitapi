using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DataContext {
    public interface IDbContextFactory<TEntity> {
        DbContext Resolve ();
    }
    public class DbContextFactory<TEntity> : IDbContextFactory<TEntity> where TEntity : class {
        private readonly ServiceProvider sp;

        //Inject possible context 
        public DbContextFactory (IServiceCollection services) {
            sp = services.BuildServiceProvider ();
        }
        public DbContext Resolve () {
            if (typeof (TEntity) == typeof (WahitHospitalContext) ||
                typeof (TEntity) == typeof (Hospital) ||
                typeof (TEntity) == typeof (HDepartment) ||
                typeof (TEntity) == typeof (Ward) ||
                typeof (TEntity) == typeof (Lab) ||
                typeof (TEntity) == typeof (HospitalStaff)
            ) {
                return sp.GetService<WahitHospitalContext> ();;
            } /* else if(typeof(x)== type(_injectdContext)){ return injectedContext} */
            else {
                return null;
            }
        }
    }
}