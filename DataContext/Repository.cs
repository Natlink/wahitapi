using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace DataContext {

    public interface IRepo<TEntity> where TEntity : class {
        Task<IEnumerable<TEntity>> Get (
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        Task<TEntity> GetById (object id);

        Task<bool> Insert (TEntity entity);
        Task<bool> Delete (object id);
        Task<bool> Delete (TEntity entityToDelete);
        Task<bool> Delete (ICollection<TEntity> entitiesToDelete);
        Task<bool> Update (TEntity entityToUpdate, string id = "");
        Task<bool> Save ();
        DbContext Context {get;set;}

        }

        /********************************************************** */

        public class Repo<TEntity> : IRepo<TEntity> where TEntity : class {
            internal DbContext _ctx;

            public Repo (IDbContextFactory<TEntity> factory) {
                this._ctx = factory.Resolve ();
                this._ctx.Database.EnsureCreated ();
            }

            public DbContext Context {
                get {
                    return _ctx;
                }
                set {_ctx = value;}
            }

            public virtual async Task<IEnumerable<TEntity>> Get (
                Expression<Func<TEntity, bool>> filter = null,
                Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                string includeProperties = "") {

                IQueryable<TEntity> query = _ctx.Set<TEntity> ();

                foreach (var includeProperty in includeProperties.Split (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
                    query = query.Include (includeProperty.Trim ());
                }

                //Detach the query
                query.AsNoTracking ();

                if (filter != null) {
                    query = query.Where (filter);
                }

                if (orderBy != null) {
                    return await orderBy (query).ToListAsync ();
                } else {
                    return await query.ToListAsync ();
                }
            }

            public virtual async Task<TEntity> GetById (object id) {

                return await _ctx.FindAsync<TEntity> (id);
            }

            public virtual async Task<bool> Insert (TEntity entity) {

                if (entity == null) {
                    throw new ArgumentNullException ("Id is null, but supposed to be set");
                }
                await _ctx.AddAsync (entity);
                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;
            }

            public virtual async Task<bool> Delete (object id) {

                var entityToDelete = await _ctx.Set<TEntity> ().FindAsync (id);
                _ctx.Remove (entityToDelete);

                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;
            }

            public virtual async Task<bool> Delete (TEntity entityToDelete) {
                if (entityToDelete == null) {
                    throw new ArgumentNullException ("Entity is null, but supposed to be set");
                }
                _ctx.Remove (entityToDelete);
                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;
            }

            public virtual async Task<bool> Delete (ICollection<TEntity> entitiesToDelete) {
                var entities = entitiesToDelete.Where (x => _ctx.Set<TEntity> ().Find (x) != null);
                if (entities == null) {
                    throw new ArgumentNullException ("Entities to delete is null, but supposed to be set");
                }
                _ctx.Set<TEntity> ().RemoveRange (entities);
                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;
            }

            public virtual async Task<bool> Update (TEntity entityToUpdate, string id = "") {
                if (entityToUpdate == null) {
                    throw new ArgumentNullException ("Entity is null and must be set");
                }
                _ctx.Entry (await _ctx.FindAsync<TEntity> (id)).State = EntityState.Deleted;
                _ctx.SaveChanges ();

                _ctx.Add (entityToUpdate);
                //_ctx.Update<TEntity>(entityToUpdate);
                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;

            }
            public virtual async Task<bool> Save () {
                if (await _ctx.SaveChangesAsync () > 0) {
                    return true;
                }
                return false;
            }

        }
    }