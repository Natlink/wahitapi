using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.EntityConfigurations {
    public class HospitalEntityConfiguration : IEntityTypeConfiguration<Hospital> {

        public void Configure (EntityTypeBuilder<Hospital> builder) {
            builder.HasChangeTrackingStrategy (ChangeTrackingStrategy.ChangingAndChangedNotificationsWithOriginalValues);
            builder.ToTable ("Hospital");
            builder.HasKey (x => x.HospitalId);
            builder.Property (x => x.About).HasMaxLength (500).IsRequired ();
            builder.Property (x => x.Name).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Photos).HasMaxLength (500);
            builder.Property (x => x.WorkingHours).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Address).WithOne (x => x.Hospital).HasForeignKey<HospitalAddress> (x => x.HospitalRef).OnDelete (DeleteBehavior.Cascade);
            builder.HasOne (x => x.Map).WithOne (x => x.Hospital).HasForeignKey<Map> (x => x.HospitalRef).OnDelete (DeleteBehavior.Cascade);
            builder.HasMany (x => x.Departments).WithOne (x => x.Hospital).IsRequired ().OnDelete (DeleteBehavior.Cascade);

        }
    }
    public class HospitalAddressEntityConfiguration : IEntityTypeConfiguration<HospitalAddress> {

        public void Configure (EntityTypeBuilder<HospitalAddress> builder) {
            builder.HasChangeTrackingStrategy (ChangeTrackingStrategy.ChangingAndChangedNotificationsWithOriginalValues);
            builder.ToTable ("Hospital_Address");
            builder.HasKey (x => x.HospitalAddressId);
            builder.Property (x => x.City).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Country).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.State).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Street).HasMaxLength (100);
        }
    }
    public class MapEntityConfiguration : IEntityTypeConfiguration<Map> {

        public void Configure (EntityTypeBuilder<Map> builder) {
            builder.HasChangeTrackingStrategy (ChangeTrackingStrategy.ChangingAndChangedNotificationsWithOriginalValues);
            builder.ToTable ("GoogleMapCordinate");
            builder.HasKey (x => x.MapId);
            builder.Property (x => x.lat).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.lng).HasMaxLength (100).IsRequired ();

        }
    }

    public class DepartmentEntityConfiguration : IEntityTypeConfiguration<HDepartment> {
        public void Configure (EntityTypeBuilder<HDepartment> builder) {
            builder.ToTable ("Department");
            // Department Configuration
            builder.HasKey (x => x.HDepartmentId);
            builder.HasOne (x => x.Hospital);
            builder.Property (x => x.About).HasMaxLength (300).IsRequired ();
            builder.Property (x => x.AssistantHOD).HasMaxLength (100);
            builder.Property (x => x.Name).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.HOD).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Hospital).WithMany (x => x.Departments).HasForeignKey (x => x.HospitalId);
            builder.HasMany (x => x.Staffs).WithOne (x => x.Department).HasForeignKey (x => x.DepartmentId).OnDelete (DeleteBehavior.SetNull);
        }
    }

    public class WardEntityConfiguration : IEntityTypeConfiguration<Ward> {
        public void Configure (EntityTypeBuilder<Ward> builder) {
            builder.ToTable ("Ward");
            builder.HasKey (x => x.WardId);
            builder.Property (x => x.Name).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Department).WithMany (x => x.Wards).HasForeignKey (x => x.DepartmentId).OnDelete (DeleteBehavior.Cascade);
        }
    }
    public class LabEntityConfiguration : IEntityTypeConfiguration<Lab> {
        public void Configure (EntityTypeBuilder<Lab> builder) {
            builder.ToTable ("Lab");
            builder.HasKey (x => x.LabId);
            builder.Property (x => x.Name).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Department).WithMany (x => x.Labs).HasForeignKey (x => x.DepartmentId).OnDelete (DeleteBehavior.Cascade);
            builder.HasMany (x => x.Technicians);
        }
    }

    public class WardRoomEntityConfiguration : IEntityTypeConfiguration<WardRoom> {
        public void Configure (EntityTypeBuilder<WardRoom> builder) {
            builder.ToTable ("WardRoom");
            builder.HasKey (x => x.WardRoomId);
            builder.Property (x => x.Number).HasMaxLength (50).IsRequired ();
            builder.HasOne (x => x.Ward).WithMany (x => x.Rooms).HasForeignKey (x => x.WardId).OnDelete (DeleteBehavior.Cascade);

        }
    }

    public class BedEntityConfiguration : IEntityTypeConfiguration<Bed> {
        public void Configure (EntityTypeBuilder<Bed> builder) {
            builder.ToTable ("Bed");
            builder.HasKey (x => x.BedId);
            builder.Property (x => x.Number).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.Location).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Room).WithMany (x => x.Beds).HasForeignKey (x => x.RoomId).OnDelete (DeleteBehavior.Cascade);

        }
    }

    public class PersonEntityConfiguration : IEntityTypeConfiguration<Person> {
        public void Configure (EntityTypeBuilder<Person> builder) {
            builder.ToTable ("Person");
            builder.HasDiscriminator<string>("Person_type").HasValue<Person>("Person_Base")
            .HasValue<Patient>("Person_Patient").HasValue<HospitalStaff>("Person_HospitalStaff");
            builder.HasKey (x => x.PersonId);
            builder.Property (x => x.BiometricScan).HasMaxLength (500).IsRequired ();
            builder.Property (x => x.BloodGroup).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.FirstName).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Gender).IsRequired ();
            builder.Property (x => x.DateOfBirth).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.LastName).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.MaritalStatus).IsRequired ();
            builder.Property (x => x.MiddleName).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.Title).HasMaxLength (50).IsRequired ();
            builder.HasOne (x => x.Contact).WithOne(x =>x.Person).HasForeignKey<Contact>(x =>x.PersonId);
            builder.Property (x => x.BloodGroup).HasMaxLength (50).IsRequired ();
        }
    }

    public class PatientEntityConfiguration : IEntityTypeConfiguration<Patient> {
        public void Configure (EntityTypeBuilder<Patient> builder) {
            builder.HasBaseType<Person> ();
            builder.Property (x => x.BedNumber).HasMaxLength (10).IsRequired ();
            builder.Property (x => x.DateAdmitted).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.DateDischarged).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.RoomNumber).HasMaxLength (50).IsRequired ();
            builder.HasOne (x => x.Bed).WithOne (x => x.Patient).HasForeignKey<Bed> (x => x.PatientId);
            builder.HasOne (x => x.Folder).WithOne (x => x.Patient).HasForeignKey<Folder> (x => x.PatientId);

        }
    }

    public class ContactEntityConfiguration : IEntityTypeConfiguration<Contact> {
        public void Configure (EntityTypeBuilder<Contact> builder) {

            builder.ToTable("Person_Address").HasKey(x =>x.ContactId);
            builder.Property (x => x.Country).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Email).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.City).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.HomePhone).HasMaxLength (100);
            builder.Property (x => x.WorkPhone).HasMaxLength (100);
            builder.Property (x => x.MobilePhone).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.State).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.HouseAddress).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.PostalCode).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Street).HasMaxLength (100);
        }
    }

    public class FolderEntityConfiguration : IEntityTypeConfiguration<Folder> {
        public void Configure (EntityTypeBuilder<Folder> builder) {
            builder.ToTable ("Folder");
            builder.HasKey (x => x.FolderId);

        }
    }

    public class LabDataEntityConfiguration : IEntityTypeConfiguration<LabData> {
        public void Configure (EntityTypeBuilder<LabData> builder) {
            builder.ToTable ("LabData");
            builder.HasKey (x => x.LabDataId);
            builder.Property (x => x.DateRecorded).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.LastModified).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Note).HasMaxLength (500).IsRequired ();
            builder.Property (x => x.TestName).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.TestResult).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Folder).WithMany (x => x.LabData).HasForeignKey (x => x.FolderId).OnDelete (DeleteBehavior.Cascade);
            builder.HasOne (x => x.Lab).WithMany (x => x.LabData).HasForeignKey (x => x.LabId).OnDelete (DeleteBehavior.Cascade);
            builder.HasOne (x => x.Technician);
        }
    }

    public class SurgeryRecordEntityConfiguration : IEntityTypeConfiguration<SurgeryRecord> {
        public void Configure (EntityTypeBuilder<SurgeryRecord> builder) {
            builder.ToTable ("SurgeryRecord");
            builder.HasKey (x => x.SurgeryRecordId);
            builder.Property (x => x.DateRecorded).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.LastModified).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Location).HasMaxLength (500).IsRequired ();
            builder.Property (x => x.TheatreName).HasMaxLength (100).IsRequired ();
            builder.HasOne (x => x.Folder).WithMany (x => x.SurgeryRecords).HasForeignKey (x => x.FolderId).OnDelete (DeleteBehavior.Cascade);
            builder.HasOne (x =>x.Surgeon).WithMany(x =>x.SurgeryRecords).HasForeignKey(x =>x.SurgeonId);
        }
    }

    public class DoctorsNoteEntityConfiguration : IEntityTypeConfiguration<DoctorsNote> {
        public void Configure (EntityTypeBuilder<DoctorsNote> builder) {
            builder.ToTable ("Prescription");
            builder.HasKey (x => x.NoteId);
            builder.Property (x => x.DateRecorded).HasMaxLength (50).IsRequired ();
            builder.Property (x => x.DateUpdated).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Prescription).HasMaxLength (1000).IsRequired ();
            builder.HasOne (x => x.Folder).WithMany (x => x.Notes).HasForeignKey (x => x.FolderId).OnDelete (DeleteBehavior.Cascade);
            builder.HasOne (x => x.Doctor);
        }
    }

    public class PatientQueueEntityConfiguration : IEntityTypeConfiguration<PatientQueue> {
        public void Configure (EntityTypeBuilder<PatientQueue> builder) {
            builder.ToTable ("PatientQueue");
            builder.HasKey (x => x.Id);
            builder.Property (x => x.TimeStarted).HasMaxLength (50).IsRequired ();
            builder.HasMany (x => x.Patients).WithOne (x => x.PatientQueue).HasForeignKey (x => x.PatientQueueId);
            builder.HasMany (x => x.Staffs).WithOne (x => x.PatientQueue).OnDelete(DeleteBehavior.SetNull);
        }
    }

    public class HospitalStaffEntityConfiguration : IEntityTypeConfiguration<HospitalStaff> {
        public void Configure (EntityTypeBuilder<HospitalStaff> builder) {
            builder.HasBaseType<Person>();
            builder.Property (x => x.Specialty).HasMaxLength (100);
            builder.Property (x => x.Shift).HasMaxLength (100).IsRequired ();
            builder.Property (x => x.Location).HasMaxLength (500).IsRequired ();
            builder.Property (x => x.WorkingHours).HasMaxLength (100).IsRequired ();

        }
    }

    ///Sammary
    // Establish the many to many relationship between hospital and Folder
    //
    public class HospitalFolderEntityConfiguration : IEntityTypeConfiguration<HospitalFolder> {
        public void Configure (EntityTypeBuilder<HospitalFolder> builder) {
            builder.ToTable ("HospitalFolderMap");
            builder.HasKey (x => new { x.FolderId, x.HospitalId });
            builder.HasOne (x => x.Hospital).WithMany (x => x.HospitalFolders).HasForeignKey (x => x.HospitalId);
            builder.HasOne (x => x.Folder).WithMany (x => x.HospitalFolders).HasForeignKey (x => x.FolderId);
        }
    }

}