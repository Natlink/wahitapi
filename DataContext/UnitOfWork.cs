using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace DataContext {

    //********************UNIT OF WORK **********************
    public interface IUnitOfWork : IDisposable {
        IRepo<TEntity> GetRepository<TEntity> () where TEntity : class;

        Task<bool> Save ();
    }

    public class UnitOfWork : IUnitOfWork {
        private readonly DbContext _ctx;
        private Dictionary<Type, object> _repositories;
        private bool _disposed;

        public UnitOfWork () {
            

            _repositories = new Dictionary<Type, object> ();
            _disposed = false;
        }

        public IRepo<TEntity> GetRepository<TEntity> () where TEntity : class {
            // Checks if the Dictionary Key contains the Model class
            if (_repositories.Keys.Contains (typeof (TEntity))) {
                // Return the repository for that Model class
                return _repositories[typeof (TEntity)] as IRepo<TEntity>;
            }

            // If the repository for that Model class doesn't exist, create it
            var repository = new Repo<TEntity> (null);

            // Add it to the dictionary
            _repositories.Add (typeof (TEntity), repository);

            return repository;
        }

        public async Task<bool> Save () {

            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;

        }
        protected virtual void Dispose (bool disposing) {
            if (!this._disposed) {
                if (disposing) {
                    _ctx.Dispose ();

                }
            }
            this._disposed = true;
        }

        public void Dispose () {
            Dispose (true);
            GC.SuppressFinalize (this);
        }
    }

    //********************END OF UNIT OF WORK*******************

}