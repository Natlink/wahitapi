﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Commons;
using DataContext.EntityConfigurations;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace DataContext {
    public interface IDbContext {

        DbSet<Hospital> Hospitals { get; set; }
        DbSet<HDepartment> HDepartments { get; set; }
        DbSet<HospitalAddress> HospitalAddresses { get; set; }
        DbSet<Map> Maps { get; set; }

        DbSet<T> Set<T> () where T : class;
        EntityEntry<T> Entry<T> (T entity) where T : class;
        // Requires in development time
        DatabaseFacade Database { get; }
        Task<int> SaveChangesAsync (CancellationToken cancellationToken = default (CancellationToken));
        Task<int> SaveChangesAsync (bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default (CancellationToken));

        ChangeTracker ChangeTracker { get; }

        EntityEntry Update (object entity);
        EntityEntry<TEntity> Update<TEntity> (TEntity entity) where TEntity : class;
        void UpdateRange (IEnumerable<object> entities);

        EntityEntry Add (object entity);
        Task<EntityEntry> AddAsync (object entity, CancellationToken cancellationToken = default (CancellationToken));
        void AddRange (IEnumerable<object> entities);

        EntityEntry Remove (object entity);
        void RemoveRange (IEnumerable<object> entities);
        Task<TEntity> FindAsync<TEntity> (params object[] keyValues) where TEntity : class;
        void Dispose ();

    }

    /// <summary>
    /// This is the snapshot of the database. It contains the definition for the database tables. 
    /// </summary>
    public class WahitHospitalContext : DbContext, IDbContext {

        public WahitHospitalContext (DbContextOptions<WahitHospitalContext> options) : base (options) {

        }

        // Tables
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<HDepartment> HDepartments { get; set; }
        public DbSet<HospitalAddress> HospitalAddresses { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<Ward> Wards { get; set; }
        public DbSet<WardRoom> WardRooms { get; set; }
        public DbSet<Bed> Beds { get; set; }
        public DbSet<Lab> Labs { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<LabData> LabRecords { get; set; }
        public DbSet<SurgeryRecord> SurgeryRecords { get; set; }
        public DbSet<DoctorsNote> DoctorNotes { get; set; }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<PatientQueue> PatientQueues { get; set; }
        public DbSet<HospitalStaff> Staffs { get; set; }
        public DbSet<HospitalFolder> HospitalFolders { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.ApplyConfiguration<Hospital> (new HospitalEntityConfiguration ());
            modelBuilder.ApplyConfiguration<HospitalAddress> (new HospitalAddressEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Map> (new MapEntityConfiguration ());
            modelBuilder.ApplyConfiguration<HDepartment> (new DepartmentEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Ward> (new WardEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Lab> (new LabEntityConfiguration ());
            modelBuilder.ApplyConfiguration<WardRoom> (new WardRoomEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Bed> (new BedEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Person> (new PersonEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Patient> (new PatientEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Contact> (new ContactEntityConfiguration ());
            modelBuilder.ApplyConfiguration<Folder> (new FolderEntityConfiguration ());
            modelBuilder.ApplyConfiguration<LabData> (new LabDataEntityConfiguration ());
            modelBuilder.ApplyConfiguration<SurgeryRecord> (new SurgeryRecordEntityConfiguration ());
            modelBuilder.ApplyConfiguration<DoctorsNote> (new DoctorsNoteEntityConfiguration ());
            modelBuilder.ApplyConfiguration<PatientQueue> (new PatientQueueEntityConfiguration ());
            modelBuilder.ApplyConfiguration<HospitalStaff> (new HospitalStaffEntityConfiguration ());
            modelBuilder.ApplyConfiguration<HospitalFolder> (new HospitalFolderEntityConfiguration ());
        }

    }

}