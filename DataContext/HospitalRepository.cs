using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataContext {
    public interface IHospitalRepository {
        Task<IEnumerable<Hospital>> Get (
            Expression<Func<Hospital, bool>> filter = null,
            Func<IQueryable<Hospital>, IOrderedQueryable<Hospital>> orderBy = null);

        Task<Hospital> GetById (string id);
        Task<bool> Insert (Hospital hospital);
        Task<bool> Insert (ICollection<Hospital> hospitals);
        Task<bool> Delete (string id);
        Task<bool> Delete (Hospital hospital);
        Task<bool> Delete (ICollection<Hospital> hospitals);
        Task<bool> Update (Hospital hospital);
        Task<bool> Update (ICollection<Hospital> hospitals);
    }

    public class HospitalRepository : IHospitalRepository {

        internal WahitHospitalContext _ctx;
        internal DbSet<Hospital> _dbSet;
        internal IQueryable<Hospital> _dbSetQ;
        public HospitalRepository (IDbContextFactory<WahitHospitalContext> factory) {
            _ctx = factory.Resolve () as WahitHospitalContext;
            _ctx.Database.EnsureCreated ();

        }
        public async Task<bool> Delete (string id) {
            if (string.IsNullOrEmpty (id)) {
                throw new ArgumentNullException ("Id is null, but supposed to be set");
            }
            var hos = await _dbSet.FindAsync (id);
            if (hos == null) {
                return false;
            }

            _dbSet.Remove (hos).State = EntityState.Deleted;
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;
        }
        public async Task<bool> Delete (Hospital hospital) {
            if (hospital == null || string.IsNullOrEmpty (hospital.HospitalId)) {
                throw new ArgumentNullException ("Hospital to delete cannot be null or must have a valid Id");
            }
            _dbSet.Remove (hospital).State = EntityState.Deleted;
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;

        }
        public async Task<bool> Delete (ICollection<Hospital> hospitals) {

            if (hospitals == null) {
                throw new ArgumentNullException ("Hospitals to delete cannot be null or must have a valid Id");
            }
            _dbSet.RemoveRange (hospitals);
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;

        }
        public async Task<IEnumerable<Hospital>> Get (Expression<Func<Hospital, bool>> filter, Func<IQueryable<Hospital>, IOrderedQueryable<Hospital>> orderBy) {
            IQueryable<Hospital> query = _ctx.Hospitals.Include ("Address")
                .Include ("Map").AsNoTracking ().AsNoTracking ();
            if (filter != null) {
                query = query.Where (filter);
            }
            if (orderBy != null) {
                return await orderBy (query).ToListAsync ();
            } else {
                return await query.ToListAsync ();
            }
        }
        public async Task<Hospital> GetById (string id) {
            if (string.IsNullOrEmpty (id)) {
                throw new ArgumentNullException ("Id is null, but supposed to be set");
            }

            return await _ctx.Hospitals
                .Include ("Address")
                .Include ("Map").AsNoTracking ().SingleAsync (x => x.HospitalId == id);

        }

        public async Task<bool> Insert (Hospital hospital) {
            if (hospital == null) {
                throw new ArgumentNullException ("Id is null, but supposed to be set");
            }
            await _ctx.AddAsync (hospital);
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;

        }

        public async Task<bool> Insert (ICollection<Hospital> hospitals) {
            if (hospitals == null) {
                throw new ArgumentNullException ("Hospitals can is null and must be set");
            }
            await _ctx.AddRangeAsync (hospitals);
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;

        }

        public async Task<bool> Update (ICollection<Hospital> hospitals) {

            if (hospitals == null) {
                throw new ArgumentNullException ("Hospitals can is null and must be set");
            }
            _ctx.UpdateRange (hospitals);
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;
        }

        public async Task<bool> Update (Hospital hospital) {
            if (hospital == null) {
                throw new ArgumentNullException ("Hospitals can is null and must be set");
            }
            _ctx.Update (hospital);
            if (await _ctx.SaveChangesAsync () > 0) {
                return true;
            }
            return false;
        }
    }
}