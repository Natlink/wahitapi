using System;
using Entities;
using Microsoft.Extensions.DependencyInjection;
namespace DataContext {
    public class DbInitializer {
        WahitHospitalContext _ctx;
        public DbInitializer (IServiceCollection services) {
            var sp = services.BuildServiceProvider ();
            _ctx = sp.GetService<WahitHospitalContext> ();
        }
        public void Seed () {
            try {
                if (_ctx.Hospitals != null) return;

                // var uow = _uowFactory.GetUnitOfWork ();
                // var hosRepo = uow.GetRepository<Hospital> ();

                var hosAdd = new HospitalAddress {
                    Country = "Ghana",
                    State = "Ash",
                    City = "Koforidua",
                    Street = "Nana Yaw's street",
                };

                var hospital = new Hospital {
                    Name = "Central Hospital",
                    Photos = "centralHospital.png",
                    Departments = null,
                    Address = hosAdd,
                    // Labs = null,
                    WorkingHours = "2:00 - 3:30",
                    About = "have faculty or departments",
                    Map = new Map { lat = "342343", lng = "342314343" }
                };


                _ctx.Database.EnsureCreated ();
                _ctx.Add (hospital);
                _ctx.SaveChanges ();
            }catch(Exception e){
                Console.WriteLine(e.Message);
            }
        }
    }
}