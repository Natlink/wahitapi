 using System.Linq;
 using System;

 //using Xunit;
 using NUnit.Framework;
 //using Moq;
 using System.Collections.Generic;
 using System.Collections;
 using System.Diagnostics;
 using DataContext;
 using DataService;
 using Entities;
 using Microsoft.EntityFrameworkCore;
 using NSubstitute;

 namespace WahitTest {

     [TestFixture]
     public class LabServiceTest {

         private IRepo<Lab> _repoMock;
         private IHospitalService _hosService;
         private IHDepartmentService _departmentService;
         private ILabService _labService;
         private DbContext _ctx;

         [SetUp]
         public void Before () {
             _repoMock = Substitute.For<IRepo<Lab>> ();
             _repoMock.Context.Returns (_ctx);
             _hosService = Substitute.For<IHospitalService> ();
             _departmentService = Substitute.For<IHDepartmentService> ();
             _hosService.GetHospitalById ("hos12345").Returns (new Hospital { HospitalId = "hos12345" });
             _departmentService.GetHDepartmentById ("3435r3435").Returns (new HDepartment { HDepartmentId = "3435r3435" });

             _labService = new LabService (_repoMock, _departmentService);
         }

         [TearDown]
         public void After () {
             _repoMock = null;
             _hosService = null;
             _departmentService = null;
             _labService = null;
         }

         [TestCase]
         public void CreateLabShouldCallSaveWhenGivenValidLabObject () {
             //Arrange
             var lab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 2,
                 DepartmentId = "3435r3435"
             };
             _departmentService.GetHDepartmentById (lab.DepartmentId).Returns (new HDepartment {Labs=new List<Lab>()});
            //_labService.GetLabByName(lab.Name,lab.DepartmentId).Returns(null)
             //Act
             _labService.CreateLab (lab);

             //Assert
             _repoMock.Received ().Context.AddAsync (lab);
         }

         [TestCase]
         public void Lab_should_be_added_to_existing_department () {
             //Arrange
             var lab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 4,
                 DepartmentId = "" //No department
             };

             //Act
             _labService.CreateLab (lab);

             //Assert
             _repoMock.DidNotReceive ().Context.AddAsync (lab);
         }

         public void Department_should_not_contain_labs_of_same_name () {
             //Arrange
             var lab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 3,
                 DepartmentId = "3435r3435"
             };

             _labService.GetLabByName (lab.Name, lab.DepartmentId).Returns (new Lab ());

             //Act
             _labService.CreateLab (lab);

             //Assert
             _repoMock.DidNotReceive ().Context.AddAsync (lab);
         }

         [TestCase]
         public void Type_of_lab_must_be_specified_during_creation () {
             //Arrange
             var lab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 0,
                 DepartmentId = "3435r3435"

             };

             //Act
             _labService.CreateLab (lab);

             //Assert
             _repoMock.DidNotReceive ().Context.AddAsync (lab);
         }

         public void Update_lab_should_modify_the_existing_lab () {
             // Arrange
             var lab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 8,
                 DepartmentId = "3435r3435",

             };

             var modifiedLab = new Lab {
                 Name = "Alfred Boating lab",
                 LabId = 9,
                 DepartmentId = "3343dfasdf3fadf",

             };

             _labService.GetLabById (5, "3343dfasdf3fadf").Returns (modifiedLab);

             //Act
             var isUpdated = _labService.UpdateLab (modifiedLab);

             //Assert
             Assert.AreNotEqual (lab.GetHashCode (), _labService.GetLabById (5, "3343dfasdf3fadf").GetHashCode ());
         }

         [TestCase]
         public void Update_lab_should_only_accept_lab_with_labId_specified () {
                 // Arrange
                 var modifiedLab = new Lab {
                     Name = "Alfred Boating lab",
                     LabId = 7,
                     DepartmentId = "3435r3435",
                 };

                 _departmentService.GetHDepartmentById ("3435r3435").Returns (new HDepartment ());

                 //Act
                 var isUpdated = _labService.UpdateLab (modifiedLab);

                 //Assert
                 Assert.AreEqual (false, isUpdated.Result);
             }
             [TestCase]
         public void Update_lab_should_only_accept_lab_with_departmentId_specified () {
             // Arrange

             var modifiedLab = new Lab {
                 Name = "Alfred  lab",
                 LabId = 8,
                 DepartmentId = "",

             };
             _departmentService.GetHDepartmentById (modifiedLab.DepartmentId).Returns (new HDepartment ());

             //Act
             var isUpdated = _labService.UpdateLab (modifiedLab);

             //Assert
             Assert.AreEqual (false, isUpdated.Result);
         }

         public void Delete_lab_takes_departmentId_as_arg_and_should_not_be_null_or_empty () {
             // Arrange

             //Act
             var isDeleted = _labService.DeleteLab (5, "");

             //Assert
             Assert.AreEqual (false, isDeleted.Result);
         }
         public void Delete_lab_takes_labId_as_arg_and_should_not_be_less_than_0 () {
             // Arrange

             //Act
             var isDeleted = _labService.DeleteLab (-4, "jldfsjldf243");

             //Assert
             Assert.AreEqual (false, isDeleted.Result);
         }

         public void GetLabByName_should_return_null_if_departmentId_is_null_or_empty () {
             //Arrange
             //Act
             var lab = _labService.GetLabByName ("YF lab", "");
             //Assert
             Assert.That (lab == null, "DepartmentId must be specified");

         }

         public void GetLabByName_should_return_null_if_labName_is_null_or_empty () {
             //Arrange
             _departmentService.GetHDepartmentById ("lajdkfjdfja", "").Returns (new HDepartment ());
             //Act
             var lab = _labService.GetLabByName ("", "lajdkfjdfja");
             //Assert
             Assert.That (lab == null, "Lab Name must be specified");

         }

         public void GetLabById_should_return_null_if_labId_is_less_than_0 () {
             //Arrange
             _departmentService.GetHDepartmentById ("lajdkfjdfja", "").Returns (new HDepartment ());
             //Act
             var isLab = _labService.GetLabById (-1, "lajdkfjdfja") != null?true : false;
             //Assert
             Assert.That (isLab == false, "Name must be specified");

         }

         public void GetLabsByDepartment_should_return_List_of_labs_and_null_if_none_is_found () {
             //Arrange

             //Act
             _labService.GetLabsByDepartment ("4549jfejri93");

             //Assert
             _repoMock.Received ().Get (w => w.DepartmentId == "4549jfejri93", null, "LabData,Technicians");
         }

         public void GetLabsByHospital_should_return_List_of_labs () {

             //Act
             _labService.GetAllLabs ();

             //Assert
             _repoMock.Received ().Get (null, null, "LabData,Technicians");

         }

     }

 }