 using System.Linq;
 using System;

 //using Xunit;
 using NUnit.Framework;
 //using Moq;
 using System.Collections.Generic;
 using System.Collections;
 using System.Diagnostics;
 using DataContext;
 using DataService;
 using Entities;
 using Microsoft.EntityFrameworkCore.ChangeTracking;
 using Microsoft.EntityFrameworkCore;
 using NSubstitute;

 namespace WahitTest {

     [TestFixture]
     public class WardServiceTest {
         private DbContextOptions _ctxOptions;
         private DbContext _ctx;
         private IDbContextFactory<IWard> _idbFacotry;
         private IRepo<Ward> _repoMock;
         private IHospitalService _hosService;
         private IHDepartmentService _departmentService;
         private IWardService _wardService;

         [SetUp]
         public void Before () {
             _ctx = Substitute.For<DbContext> ();
             // _idbFacotry.Resolve ().Returns (_ctx);
             _repoMock = Substitute.For<IRepo<Ward>> ();
             _repoMock.Context.Returns (_ctx);
             _hosService = Substitute.For<IHospitalService> ();
             _departmentService = Substitute.For<IHDepartmentService> ();
             _hosService.GetHospitalById ("hos12345").Returns (new Hospital { HospitalId = "hos12345" });
             _departmentService.GetHDepartmentById ("dept12345", "").Returns (new HDepartment { HDepartmentId = "dept12345" });

             _wardService = new WardService (_repoMock, _departmentService);
         }

         [TearDown]
         public void After () {
             _repoMock = null;
             _hosService = null;
             _departmentService = null;
             _wardService = null;
         }

         [TestCase]
         public void CreateWardShouldCallSaveWhenGivenValidWardObject () {
             //Arrange
             var ward = new Ward {
                 Name = "Alfred Boating ward",
                 Type = WardType.Female,
                 DepartmentId = "3435r3435"
             };
             _departmentService.GetHDepartmentById ("3435r3435").Returns (new HDepartment ());

             //Act
             _wardService.CreateWard (ward);

             //Assert
             _repoMock.Received ().Context.AddAsync (ward);
             
         }

         [TestCase]
         public void CreateWard_should_add_ward_to_existing_department () {
             //Arrange
             var ward = new Ward {
                 Name = "Alfred Boating ward",
                 WardId = "3343dfasdf3fadf",
                 Type = WardType.Female,
                 DepartmentId = "" //No department
             };
             _departmentService.GetHDepartmentById ("3435r3435").Returns (new HDepartment ());

             //Act
             _wardService.CreateWard (ward);

             //Assert
             _repoMock.DidNotReceive ().Context.AddAsync (ward);
         }

         public void Department_should_not_contain_wards_of_same_name () {
             //Arrange
             var ward = new Ward {
                 Name = "Alfred Boating ward",
                 WardId = "3343dfasdf3fadf",
                 Type = WardType.Female,
                 DepartmentId = "3435r3435"
             };
             _wardService.GetWardByName (ward.Name,ward.DepartmentId).Returns (ward);

             //Act
             _wardService.CreateWard (ward);

             //Assert
             _repoMock.DidNotReceive ().Context.Entry (ward);
         }

         [TestCase]
         public void Type_of_ward_must_be_specified_during_creation () {
             //Arrange
             var ward = new Ward {
                 Name = "Alfred Boating ward",
                 WardId = "3343dfasdf3fadf",
                 DepartmentId = "3435r3435"

             };

             //Act
             _wardService.CreateWard (ward);

             //Assert
             _repoMock.DidNotReceive ().Insert (ward);
         }

         //  [TestCase]
         //  public void Update_ward_should_modify_the_existing_ward () {
         //      // Arrange
         //      var ward = new Ward {
         //          Name = "Alfred Boating ward",
         //          WardId = "3343dfasdf3fadf",
         //          DepartmentId = "3435r3435",
         //          Type = WardType.Female,

         //      };

         //      var modifiedWard = new Ward {
         //          Name = "Alfred Boating ward",
         //          WardId = "3343dfasdf3fadf",
         //          DepartmentId = "3435r3435",
         //          Type = WardType.Male

         //      };

         //      _wardService.GetWardById ("3343dfasdf3fadf", "3435r3435").Returns (modifiedWard);

         //      //Act
         //      var isUpdated = _wardService.UpdateWard (modifiedWard);

         //      //Assert
         //      Assert.AreNotEqual (ward.GetHashCode (), _wardService.GetWardById ("3343dfasdf3fadf", "3435r3435").GetHashCode ());
         //  }

         [TestCase]
         public void Update_ward_should_only_accept_ward_with_wardId_specified () {
             // Arrange
             var modifiedWard = new Ward {
                 Name = "Alfred Boating ward",
                 WardId = "",
                 DepartmentId = "3435r3435",
                 Type = WardType.Male

             };

             _departmentService.GetHDepartmentById ("3435r3435").Returns (new HDepartment ());

             //Act
             var isUpdated = _wardService.UpdateWard (modifiedWard);

             //Assert
             _repoMock.DidNotReceive ().Context.Entry (modifiedWard);
         }

         [TestCase]
         public void Update_ward_should_only_accept_ward_with_departmentId_specified () {
             // Arrange

             var modifiedWard = new Ward {
                 Name = "Alfred Boating ward",
                 WardId = "3343dfasdf3fadf",
                 DepartmentId = "",
                 Type = WardType.Male

             };

             _departmentService.GetHDepartmentById ("").Returns (new HDepartment ());

             //Act
             var isUpdated = _wardService.UpdateWard (modifiedWard);

             //Assert
             _repoMock.DidNotReceive ().Context.Entry (modifiedWard);
         }

         public void Delete_ward_takes_departmentId_as_arg_and_should_not_be_null_or_empty () {
             // Arrange

             //Act
             var isDeleted = _wardService.DeleteWard ("3452354r254", "");
             isDeleted = _wardService.DeleteWard ("3452354r254", null);

             //Assert
             Assert.AreEqual (false, isDeleted.Result);
         }
         public void Delete_ward_takes_wardId_as_arg_and_should_not_be_null_or_empty () {
             // Arrange

             //Act
             var isDeleted = _wardService.DeleteWard ("", "jldfsjldf243");
             isDeleted = _wardService.DeleteWard (null, "jldfsjldf243");

             //Assert
             Assert.AreEqual (false, isDeleted.Result);
         }

         public void GetWardByName_should_return_null_if_departmentId_is_null_or_empty () {
             //Arrange
             //Act
             var ward = _wardService.GetWardByName ("YF ward", "");
             //Assert
             Assert.That (ward == null, "DepartmentId must be specified");

         }

         public void GetWardByName_should_return_null_if_wardName_is_null_or_empty () {
             //Arrange
             _departmentService.GetHDepartmentById ("lajdkfjdfja", "").Returns (new HDepartment ());
             //Act
             var ward = _wardService.GetWardByName ("YF ward", "lajdkfjdfja");
             //Assert
             Assert.That (ward == null, "Ward name must be specified");

         }

         public void GetWardById_should_return_null_if_wardId_is_null_or_empty () {
             //Arrange
             _departmentService.GetHDepartmentById ("lajdkfjdfja", "").Returns (new HDepartment ());
             //Act
             var isWard = true;
             isWard = _wardService.GetWardById ("", "lajdkfjdfja") != null?true : false;
             isWard = _wardService.GetWardById (null, "lajdkfjdfja") != null?true : false;
             //Assert
             Assert.That (isWard == false, "WardId must be specified");

         }

         public void GetWardsByDepartment_should_return_List_of_wards_and_null_if_none_is_found () {
             //Arrange

             //Act
             _wardService.GetWardsByDepartment ("4549jfejri93");

             //Assert
             _repoMock.Received ().Get (w => w.DepartmentId == "4549jfejri93", null, "Rooms");
         }

         public void GetWardsByHospital_should_return_List_of_wards () {

             //Act
             _wardService.GetAllWards ();

             //Assert
             _repoMock.Received ().Get (null, null, "Rooms");

         }

     }

 }