 using System.Collections.Generic;
 using System.Diagnostics;
 using System.Linq;
 using System;
 using DataContext;
 using DataService;
 using Entities;
 using NSubstitute;
 using NUnit.Framework;

 namespace WahitTest {
     [TestFixture]
     public class HospitalServiceTest {
         private IHospitalService _hosService;
         private IDbContext _dbCtxMock;

         private IRepo<Hospital> _repoMock;
         private ICollection<IHospital> _hospitalList;

         [SetUp]
         public void Before () {
                 _hospitalList = new List<IHospital> ();
                 _dbCtxMock = Substitute.For<IDbContext> ();
                 _repoMock = Substitute.For<IRepo<Hospital>> ();
                 _hosService = new HospitalService (_repoMock, null);

             }
             [TearDown]
         public void After () {

         }

         [TestCase]
         public void AddNewHopitalShouldMakeCallToSave () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 Photos = "centralHospital.png",
                 Departments = null,
                 Address = new HospitalAddress {
                 Country = "Ghana",
                 State = "Ash",
                 City = "Koforidua",
                 Street = "Nana Yaw's street"
                 },
                 Map = new Map { lat = "34.44545454543", lng = "0.3473874647387" },
                 WorkingHours = "2:00 - 3:30",
                 About = "have faculty or departments",
             };

             //Act 
             _hosService.AddHospital (newHospital);

             //Assert
             _repoMock.Received ().Insert (newHospital);

         }

         [TestCase]
         public void AddNewHospitalWithoutNameShouldNotThrowException () {
             //Arrange
             var newHospital = new Hospital {
                 Name = ""
             };
             //Act 

             //Assert
             // Assert.Throws<ArgumentException> (() =>);
             Assert.DoesNotThrow (() => _hosService.AddHospital (newHospital));
         }

         [TestCase]
         public void AddNewHospitalWithoutNameShouldReturnFalse () {
             //Arrange
             var newHospital = new Hospital {
                 Name = ""
             };
             //Act 

             //Assert
             Assert.AreEqual (false, _hosService.AddHospital (newHospital).Result);
         }

         [TestCase]
         public void AddHospitalShouldPreventHospitalDuplicationByAddress () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 About = "",
                 Address = new HospitalAddress ()
             };
             IHospitalService hosService = Substitute.For<IHospitalService> ();
             hosService.GetHospitalByAddress (newHospital.Address).Returns (newHospital);
             //Act 

             //Assert
             Assert.AreEqual (false, hosService.AddHospital (newHospital).Result);
         }

         [TestCase]
         public void AddHospitalShouldPreventHospitalsWithNullAddressToBeSaved () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 About = "",
                 Address = null
             };
             IHospitalService hosService = Substitute.For<IHospitalService> ();
             hosService.GetHospitalByAddress (newHospital.Address).Returns (newHospital);
             //Act 

             //Assert
             Assert.AreEqual (false, hosService.AddHospital (newHospital).Result);
         }

         [TestCase]
         public void AddHospitalShouldPreventHospitalsWithEmptyAddressToBeSaved () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 About = "",
                 Address = new HospitalAddress ()
             };
             //Act 

             //Assert
             Assert.AreEqual (false, _hosService.AddHospital (newHospital).Result);
         }

         [TestCase]
         public void UpdateHospitalShouldTakeAnArgThatHaveASpecifiedID () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 Photos = "centralHospital.png",
                 Departments = null,
                 Address = new HospitalAddress {
                 Country = "Ghana",
                 State = "Ash",
                 City = "Koforidua",
                 Street = "Nana Yaw's street",
                 },
                 WorkingHours = "2:00 - 3:30",
                 About = "have faculty or departments",
             };

             //Act 

             //Assert
             Assert.AreEqual (false, _hosService.UpdateHospital (newHospital).Result);
         }

         [TestCase]
         public void UpdateHospitalShouldOnlyUpdateExistingHospitals () {
             //Arrange
             var newHospital = new Hospital {
                 Name = "Central Hospital",
                 Photos = "centralHospital.png",
                 Address = new HospitalAddress {
                 Country = "Ghana",
                 State = "Ash",
                 City = "Koforidua",
                 Street = "Nana Yaw's street",
                 },
                 WorkingHours = "2:00 - 3:30",
                 About = "have faculty or departments",
             };

             var hosService = Substitute.For<IHospitalService> ();
             hosService.GetHospitalById ("1234334").Returns (newHospital);
             //Act 

             //Assert
             Assert.AreEqual (false, hosService.UpdateHospital (newHospital).Result);
         }

         [TestCase]
         public void DeleteHospitalByIdShouldTakeAValidId () {
             //Arrange

             //Act

             //Assert
             Assert.AreEqual (false, _hosService.DeleteHospitalById (null).Result);
             Assert.AreEqual (false, _hosService.DeleteHospitalById ("").Result);
         }

         [TestCase]
         public void GetHospitalByIdShouldReturnNullForEmptyIdArg () {
             //Arrange

             //Act

             //Assert
             Assert.AreEqual (null, _hosService.GetHospitalById ("").Result);
             Assert.AreEqual (null, _hosService.GetHospitalById (null).Result);
         }

         [TestCase]
         public void GetHospitalByAddressShouldReturnNullForNullOrEmptyArg () {
             //Arrange

             //Act

             //Assert
             Assert.AreEqual (null, _hosService.GetHospitalByAddress (new HospitalAddress ()).Result);
             Assert.AreEqual (null, _hosService.GetHospitalByAddress (null).Result);
         }

     }
 }