 using System.Linq;
 using System;

 //using Xunit;
 using NUnit.Framework;
 //using Moq;
 using System.Collections.Generic;
 using System.Collections;
 using System.Diagnostics;
 using DataContext;
 using DataService;
 using Entities;
 using Microsoft.EntityFrameworkCore;
 using NSubstitute;

 namespace WahitTest {

     [TestFixture]
     public class HospitalDepartmentServiceTest {

         private IRepo<HDepartment> _repoMock;
         private IHospitalService _hosService;
         private IHDepartmentService _deptService;
         private DbContext _ctx;
         [SetUp]
         public void Before () {
             _repoMock = Substitute.For<IRepo<HDepartment>> ();
             _repoMock.Context.Returns (_ctx);
             _hosService = Substitute.For<IHospitalService> ();
             _deptService = new HDepartmentService (_repoMock, _hosService);
             _hosService.GetHospitalById ("").Returns (new Hospital ());

         }

         [TearDown]
         public void After () {
             _repoMock = null;
             _hosService = null;
         }

         [TestCase]
         public void CreateHDepartmentShouldCallSaveWhenGivenValidDeptObject () {
                 //Arrange
                 _hosService.GetHospitalById ("2343faeqreqr34").Returns (new Hospital ());

                 var dept = new HDepartment {
                     AssistantHOD = "Akwasi Andrews",
                     HOD = "Yaw Sarfo",
                     Name = "Dentistry",
                     About = "We don't about your teeth"
                 };

                 //Act
                 _deptService.CreateHospitalDepartment ("2343faeqreqr34", dept);

                 //Assert
                 _repoMock.Received ().Context.AddAsync (dept);
             }
             [TestCase]
         public void CreateHDepartmentShouldNotAcceptEmptyHospitalId () {
             //Arrange
             var dept = new HDepartment {
                 AssistantHOD = "Akwasi Andrews",
                 HOD = "Yaw Sarfo",
                 Name = "Dentistry",
                 About = "We don't about your teeth"
             };

             //Act
             _deptService.CreateHospitalDepartment ("", dept);

             //Assert
             //   _uow.DidNotReceive ().Save ();
         }

         [TestCase]
         public void UpdateHDepartmentShouldHaveHOD () {
             //Arrange
             var dept = new HDepartment {
                 AssistantHOD = "Akwasi Andrews",
                 HOD = "",
                 Name = "Dentistry",
                 About = "We don't care about your teeth",
                 HDepartmentId = "dlsfj33l4"
             };

             //Act
             var isUpdated = _deptService.UpdateHospitalDepartment (dept);

             //Assert
             Assert.AreEqual (isUpdated.Result, false);
             // _uow.DidNotReceive ().Save ();
         }

         [TestCase]
         public void UpdateHDepartmentShouldHaveAbout () {
             // Arrange
             var dept = new HDepartment {
                 AssistantHOD = "Akwasi Andrews",
                 HOD = "Alfred ",
                 Name = "Dentistry",
                 About = "",
                 HDepartmentId = "dlsfj33l4"
             };

             //Act
             var isUpdated = _deptService.UpdateHospitalDepartment (dept);

             //Assert
             Assert.AreEqual (isUpdated.Result, false);
             // _uow.DidNotReceive ().Save ();
         }

         [TestCase]
         public void DeleteHDepartmentMustNotHaveEmptyHospitalIdAsArg () {
             // Arrange

             //Act
             var isDeleted = _deptService.DeleteHospitalDepartment ("", "dlsfj33l4");

             //Assert
             Assert.AreEqual (isDeleted.Result, false);
             // _uow.DidNotReceive ().Save ();
         }

         [TestCase]
         public void DeleteHDepartmentMustNotHaveEmptyDepartmentId () {
             // Arrange

             //Act
             var isDeleted = _deptService.DeleteHospitalDepartment ("daflkdsaadfl", "");

             //Assert
             Assert.AreEqual (isDeleted.Result, false);
             // _uow.DidNotReceive ().Save ();
         }

         [TestCase]
         public void DeleteHDepartmentMustReturnFalseIfDepartmentDoesNotExist () {
             // Arrange

             //Act
             var isDeleted = _deptService.DeleteHospitalDepartment ("daflkdsaadfl", "lsdjflsdfd");

             //Assert
             Assert.AreEqual (false, isDeleted.Result);
             // _uow.DidNotReceive ().Save ();
         }

         public void GetDepartmentByIdMustHaveAHospitalIdElseReturnNull () {
             // Arrange
             var dept = new HDepartment {
                 HDepartmentId = "lsdjflsdfd",
                 AssistantHOD = "Akwasi Andrews",
                 HOD = "Alfred ",
                 Name = "Dentistry",
                 About = ""
             };

             //Act
             var dpt = _deptService.GetHDepartmentById ("", dept.HDepartmentId);

             //Assert
             Assert.AreEqual (null, dpt);

         }

         public void GetDepartmentByIdMustHaveADepartmentIdElseReturnNull () {
             // Arrange
             var dept = new HDepartment {
                 HDepartmentId = "lsdjflsdfd",
                 AssistantHOD = "Akwasi Andrews",
                 HOD = "Alfred ",
                 Name = "Dentistry",
                 About = ""
             };

             //Act
             var dpt = _deptService.GetHDepartmentById ("ksdjfdjflds", "");

             //Assert
             Assert.AreEqual (null, dpt);

         }
     }

 }