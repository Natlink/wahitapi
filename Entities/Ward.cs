﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {
    public interface IWard {
        string WardId { get; set; }
         WardType Type { get; set; }
        string Name { get; set; }
        ICollection<WardRoom> Rooms { get; set; }
        string DepartmentId { get; set; }
        HDepartment Department { get; set; }
        void AddRoom(WardRoom room);
    }

    public class Ward : IWard {
        public Ward () {
            Rooms = new ObservableHashSet<WardRoom> ();
        }

        public  string WardId { get; set; }
        public  WardType Type { get; set; }
        public  string Name { get; set; }
        public string DepartmentId { get; set; }
        public  HDepartment Department { get; set; }
        public  ICollection<WardRoom> Rooms { get; set; }
        public  void AddRoom(WardRoom room)
         {
             room.Ward = this;
             Rooms.Add(room);
         }
    }
}