﻿namespace Entities
{
 //Component of Person  
    public class Contact
    {
        public int ContactId{get; set;}
        public virtual string Country { get; set; }
        public virtual string Email { get; set; }
        public virtual string MobilePhone { get; set; }
        public virtual string WorkPhone { get; set; }
        public virtual string HomePhone { get; set; }
        public virtual string State { get; set; }
        public virtual string City { get; set; }
        public virtual string HouseAddress { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Street { get; set; }

        public Person Person{get; set;}
        public string PersonId{get;set;}

        public bool IsValid(){
            if(
                string.IsNullOrEmpty(Country)||
                string.IsNullOrEmpty(Email)||
                string.IsNullOrEmpty(MobilePhone)||
                string.IsNullOrEmpty(State)||
                string.IsNullOrEmpty(City)||
                string.IsNullOrEmpty(HouseAddress)||
                string.IsNullOrEmpty(PostalCode)
            )return false;

            return true;
        }
    }
}
