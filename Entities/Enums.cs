namespace Entities {
    public enum Gender {
        Male,
        Female
    }
    public enum MaritalStatus {
        Single,
        Married,
        Widow,
        Divorce
    }

    public enum WardType {
        Male,
        Female
    }

    public enum WahitStaffType {

    }
    public enum HospitalStaffType {
        Doctor,
        Nurse,
        SeniorNurse,
        LabTechnician,
        Pharmacist,
        Therapist,
    }
    

    public enum EventCategory {
        Public,
        Private,
        Admin,
        WahitAdmin,
        HospitalAdmin,
        Hospital
    }

}