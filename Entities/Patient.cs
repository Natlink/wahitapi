﻿using System;
using System.Collections.Generic;

namespace Entities {
    public interface IPatient : IPerson {
        string BedNumber { get; set; }
        string DateAdmitted { get; set; }
        string DateDischarged { get; set; }
        string RoomNumber { get; set; }
        int? PatientQueueId { get; set; }
        PatientQueue PatientQueue { get; set; }
        Bed Bed { get; set; }
        Folder Folder { get; set; }

        bool Equals (object other);

        // null or not a cat
        int GetHashCode ();

        bool IQueue ();
    }

    public class Patient : Person, IPatient {
        public Patient () {
            DateAdmitted = DateTime.Now.ToString ();
            DateDischarged = DateTime.Now.ToString ();
        }

        public virtual string BedNumber { get; set; }
        public virtual string DateAdmitted { get; set; }
        public virtual string DateDischarged { get; set; }
        public virtual string RoomNumber { get; set; }
        public int? PatientQueueId { get; set; }
        public PatientQueue PatientQueue { get; set; }
        public virtual Bed Bed { get; set; }
        public virtual Folder Folder { get; set; }

        public override bool Equals (object other) {
            if (this == other) return true;

            var patient = other as Patient;
            if (patient == null) return false; // null or not a cat

            if (FirstName != patient.FirstName ||
                LastName != patient.LastName ||
                MiddleName != patient.MiddleName ||
                DateOfBirth != patient.DateOfBirth ||
                BiometricScan != patient.BiometricScan ||
                Gender != patient.Gender) return false;
            return true;
        }

        public override int GetHashCode () {
            unchecked {
                int result;
                result = PersonId.GetHashCode ();
                result = 29 * result + DateOfBirth.GetHashCode ();
                return result;
            }
        }

        public bool IQueue () {
            if (this.PatientQueue != null) {
                return true;
            }
            return false;
        }

    }
}