﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
namespace Entities {
    public class PatientQueue {
        public PatientQueue () {
            Patients = new ObservableHashSet<Patient> ();
            Staffs = new ObservableHashSet<HospitalStaff> ();
        }
        public int Id { get; set; }
        public string TimeStarted { get; set; }

        public ICollection<Patient> Patients { get; set; }
        public ICollection<HospitalStaff> Staffs { get; set; }
    }
}