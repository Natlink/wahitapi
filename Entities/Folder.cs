﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {
    public interface IFolder {
        string FolderId { get; set; }
        ICollection<LabData> LabData { get; set; }
        ICollection<SurgeryRecord> SurgeryRecords { get; set; }
        string PatientId { get; set; }
        Patient Patient { get; set; }
        ICollection<DoctorsNote> Notes { get; set; }
        ICollection<HospitalFolder> HospitalFolders { get; set; }
        void AddNote (DoctorsNote note);
        void AddLabData (LabData data);
        void AddSurgeryRecord (SurgeryRecord record);

    }

    public class Folder : IFolder {
        public Folder () {
            Notes = new ObservableHashSet<DoctorsNote> ();
            LabData = new ObservableHashSet<LabData> ();
            SurgeryRecords = new ObservableHashSet<SurgeryRecord> ();
        }

        public virtual string FolderId { get; set; }
        public virtual ICollection<LabData> LabData { get; set; }
        public virtual ICollection<SurgeryRecord> SurgeryRecords { get; set; }
        public virtual string PatientId { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual ICollection<DoctorsNote> Notes { get; set; }
        public ICollection<HospitalFolder> HospitalFolders { get; set; }

        public void AddHospital (Hospital hospital) {
            HospitalFolders.Add(new HospitalFolder(hospital,this));
        }

        public virtual void AddLabData (LabData data) {
            data.Folder = this;
            LabData.Add (data);
        }

        public virtual void AddSurgeryRecord (SurgeryRecord record) {
            record.Folder = this;
            SurgeryRecords.Add (record);
        }
        
        public virtual void AddNote (DoctorsNote note) {
            note.Folder = this;
            Notes.Add (note);
        }
    }
}