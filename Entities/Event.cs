﻿
namespace Entities
{
   

    public interface IEvent
    {
        string OwnerId { get; set; }
        string Title { get; set; }
        string Code { get; set; }
        string Location { get; set; }
        string Note { get; set; }
        bool IsFired { get; set; }
        string ExpiredOn { get; set; }
        string DateCreated { get; set; }
        string DateUpdated { get; set; }
        //EventCategory Category { get; set; }
    }

    //Entities
   
    public class Event : IEvent
    {
        public string OwnerId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Location { get; set; }
        public string Note { get; set; }
        public bool IsFired { get; set; }
        public string ExpiredOn { get; set; }
        public string DateCreated { get; set; }
        public string DateUpdated { get; set; }
        //public EventCategory Category { get; set; }
    }

   
}
