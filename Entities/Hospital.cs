﻿using System.Collections.Generic;
using Commons;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {
    public interface IHospital {
        string HospitalId { get; set; }
        string Name { get; set; }
        string Photos { get; set; }
        ICollection<HDepartment> Departments { get; set; }
        HospitalAddress Address { get; set; }
        Map Map { get; set; }
        string WorkingHours { get; set; }
        string About { get; set; }
        ICollection<HospitalFolder> HospitalFolders { get; }
    }

    public class Hospital : NotificationEntity, IHospital {
        private string _hospitalId;
        private string _name;
        private string _photos;
        private string _workingHours;
        private string _about;
        private HospitalAddress _address;
        private Map _map;

        public Hospital () {
            Departments = new ObservableHashSet<HDepartment> ();
        }

        public string HospitalId {
            get { return _hospitalId; }
            set { SetWithNotify (value, ref _hospitalId); }
        }
        public string Name {
            get { return _name; }
            set { SetWithNotify (value, ref _name); }
        }
        public string Photos {
            get { return _photos; }
            set { SetWithNotify (value, ref _photos); }
        } //Comma separated
        public string WorkingHours {
            get { return _workingHours; }
            set { SetWithNotify (value, ref _workingHours); }
        }
        public string About {
            get { return _about; }
            set { SetWithNotify (value, ref _about); }
        }

        public virtual ICollection<HDepartment> Departments { get; set; }
        public virtual HospitalAddress Address {
            get { return _address; }
            set {
                _address = value;
            }
        }

        public virtual Map Map {
            get { return _map; }
            set { _map = value; }
        }
        public ICollection<HospitalFolder> HospitalFolders { get; set; }


         public  void AddHDepartment(HDepartment dpt)
         {
              dpt.Hospital = this;
             Departments.Add(dpt);
         }

        
         }
}