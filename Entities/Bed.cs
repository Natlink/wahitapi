﻿using System.Collections.Generic;

namespace Entities {
    //Component of Person  
    public interface IBed {
        int BedId { get; set; }
        string Number { get; set; }
        string Location { get; set; }

        string RoomId { get; set; }
        WardRoom Room { get; set; }
        Patient Patient { get; set; }
        bool Equals (Bed bed);
        bool IsValid ();
        void AddPatient (Patient patient);
        void ClearBed (Patient patient);
    }

    public class Bed : IBed {
        public int BedId { get; set; }
        public string Number { get; set; }
        public string Location { get; set; }
        public string RoomId { get; set; }
        public WardRoom Room { get; set; }
        public string PatientId { get; set; }
        public Patient Patient { get; set; }

        public bool IsValid () {
            if (string.IsNullOrEmpty (Number) &&
                string.IsNullOrEmpty (Location) &&
                string.IsNullOrEmpty (RoomId)) return true;

            return false;
        }

        public bool Equals (Bed bed) {
            if (string.Equals (Number, bed.Number) &&
                string.Equals (Location, bed.Location) &&
                string.Equals (BedId, bed.BedId) &&
                string.Equals (RoomId, bed.RoomId)) return true;

            return false;
        }
        public void AddPatient (Patient patient) {
            Patient = patient;
            patient.Bed = this;

        }

        public void ClearBed (Patient patient) {
            Patient = null;
            patient.Bed = null;
        }

    }
}