﻿using System;
using System.Collections.Generic;
using Commons;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {
    public interface IHDepartment {
        string HDepartmentId { get; set; }
        string AssistantHOD { get; set; }
        string HOD { get; set; }
        string Name { get; set; }
        string About { get; set; }
        string HospitalId { get; set; }
        Hospital Hospital { get; set; }

        ICollection<HospitalStaff> Staffs { get; set; }

        ICollection<Ward> Wards { get; set; }
        ICollection<Lab> Labs { get; set; }
        void AddStaff(HospitalStaff staff); 
        void AddWard (Ward ward);
        void AddLab (Lab lab);
    }

    public class HDepartment : IHDepartment {

        public HDepartment () {
            Wards = new ObservableHashSet<Ward> ();
            Labs = new ObservableHashSet<Lab> ();
            Staffs = new ObservableHashSet<HospitalStaff>();
        }
        public string HDepartmentId { get; set; }
        public string AssistantHOD { get; set; }
        public string HOD { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public string HospitalId { get; set; }
        public  Hospital Hospital { get; set; }

        public ICollection<Ward> Wards { get; set; }
        public ICollection<Lab> Labs { get; set; }

        public ICollection<HospitalStaff> Staffs { get; set; }

        public  void AddStaff(HospitalStaff staff)
        {
            if (staff == null) return;
            staff.Department = this;
            Staffs.Add(staff);
        }

        public virtual void AddWard (Ward ward) {
            if(ward == null)return;
            ward.Department = this;
            Wards.Add (ward);
        }

        public virtual void AddLab (Lab lab) {
            if(lab == null)return;
            lab.Department = this;
            Labs.Add (lab);
        }

    }

}