﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;
namespace Entities {
    public interface IWardRoom {
        string WardRoomId { get; set; }
        string Number { get; set; }
        ICollection<Bed> Beds { get; set; } 
        string WardId { get; set; }
        Ward Ward { get; set; }
         void AddBed(Bed bed);
         bool IsValid();
         bool Equals(object obj);

    }
    public class WardRoom {
        public WardRoom () {
                Beds = new ObservableHashSet<Bed>();
        }
        public string WardRoomId { get; set; }
        public string Number { get; set; }

        public string WardId { get; set; }
        public Ward Ward { get; set; }

        public ICollection<Bed> Beds { get; set; } 

        public void AddBed(Bed bed)
        {
            bed.Room = this;
            Beds.Add(bed);
        }

       // override object.Equals
        public override bool Equals(object obj)
        {
            //
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //
            
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            // TODO: write your implementation of Equals() here
            throw new System.NotImplementedException();
            //return base.Equals (obj);
        }
        
        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            throw new System.NotImplementedException();
            //return base.GetHashCode();
        }

        public bool IsValid(){
            if(string.IsNullOrEmpty(Number)||
            string.IsNullOrEmpty(WardRoomId)||
            string.IsNullOrEmpty(WardId))
            return false;
            return true;
        }
    }


}