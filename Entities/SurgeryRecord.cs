﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;
namespace Entities {
    public interface ISurgeryRecord {
        string SurgeryRecordId { get; set; }
        string DateRecorded { get; set; }
        string LastModified { get; set; } string Location { get; set; }
        string TheatreName { get; set; }
        HospitalStaff Surgeon { get; set; }
        string FolderId { get; set; }
        Folder Folder { get; set; }
        //ICollection<HospitalStaff> OtherStaffs { get; set; } 

    }

    public class SurgeryRecord : ISurgeryRecord {
        private HospitalStaff _surgeon;
        public SurgeryRecord () {
           // OtherStaffs = new ObservableHashSet<HospitalStaff>();
        }
        public string SurgeryRecordId { get; set; }
        public string DateRecorded { get; set; }
        public string LastModified { get; set; }
        public string Location { get; set; }
        public string TheatreName { get; set; }
        public string SurgeonId{get; set;}
        public HospitalStaff Surgeon {
            get {
                return _surgeon;
            }
            set {
                if (value.Type == HospitalStaffType.Doctor) {
                    _surgeon = value;
                }
            }
        }
        public string FolderId { get; set; }
        public Folder Folder { get; set; }
        //public virtual ICollection<HospitalStaff> OtherStaffs { get; set; }

    }
}