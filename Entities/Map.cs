using Commons;

namespace Entities {

    public interface IMap {
        string MapId { get; set; }
        string lng { get; set; }
        string lat { get; set; }
        Hospital Hospital { get; set; }
    }
    public class Map : NotificationEntity, IMap {
        private string _mapId;
        private string _lng;
        private string _lat;
        public string MapId {
            get { return _mapId; }
            set { SetWithNotify (value, ref _mapId); }
        }
        public string lng {
            get { return _lng; }
            set { SetWithNotify (value, ref _lng); }
        }
        public string lat {
            get { return _lat; }
            set { SetWithNotify (value, ref _lat); }
        }
        public string HospitalRef { get; set; }
        public virtual Hospital Hospital { get; set; }
    }
}