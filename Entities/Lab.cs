﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {
    public interface ILab {
        int LabId { get; set; }
        string Name { get; set; }
        string DepartmentId { get; set; }
        HDepartment Department { get; set; }
        ICollection<HospitalStaff> Technicians { get; set; }
        // ICollection<LabData> LabData { get; set; }
        void AddLabData (LabData data);
    }

    public class Lab : ILab {
        public Lab () {
            Technicians = new ObservableHashSet<HospitalStaff>();
            LabData = new ObservableHashSet<LabData> ();
        }
        public int LabId { get; set; }
        public string Name { get; set; }
        public string DepartmentId { get; set; }
        public HDepartment Department { get; set; }
        public  ICollection<HospitalStaff> Technicians { get; set; }
        public ICollection<LabData> LabData { get; set; }

        public virtual void AddLabData (LabData data) {
            data.Lab = this;
            LabData.Add (data);
        }
    }
}