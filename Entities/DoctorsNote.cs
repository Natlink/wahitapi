﻿namespace Entities {
    public interface IDoctorsNote {
        int NoteId { get; set; }
        string DateRecorded { get; set; }
        string DateUpdated { get; set; }
        string Prescription { get; set; }
        string FolderId { get; set; }
        Folder Folder { get; set; }
        HospitalStaff Doctor { get; set; }
    }
    public class DoctorsNote : IDoctorsNote {
        private HospitalStaff _doctor;
        public int NoteId { get; set; }
        public string DateRecorded { get; set; }
        public string DateUpdated { get; set; }
        public string Prescription { get; set; }

        public string DoctorId {get; set;}
        public virtual HospitalStaff Doctor {
            get {
                return _doctor;
            }
            set {
                if (value.Type == HospitalStaffType.Doctor) {
                    _doctor = value;
                }
            }
        }
        public string FolderId { get; set; }
        public Folder Folder { get; set; }
    }

}