﻿namespace Entities
{
    public interface IPerson
    {
         string PersonId { get; set; }
         string FirstName { get; set; }
         string LastName { get; set; }
         string MiddleName { get; set; }
         Gender Gender { get; set; }
         string DateOfBirth { get; set; }
         Contact Contact { get; set; }
         string Title { get; set; }
         MaritalStatus MaritalStatus { get; set; }
         string BloodGroup { get; set; }
         string BiometricScan { get; set; }
         string Photos { get; set; }
    }
    public  class Person: IPerson
    {
         protected Person()
        {
           Contact = new Contact();
        }
        public virtual string PersonId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual Gender Gender  { get; set; }
        public virtual string DateOfBirth { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual string Title { get; set; }
        public virtual MaritalStatus MaritalStatus { get; set; }
        public virtual string BloodGroup { get; set; }
        public virtual string BiometricScan { get; set; }
        public virtual string Photos { get; set; }

    }
}
