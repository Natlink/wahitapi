﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Entities {

    public interface IHospitalStaff : IPerson {
        HospitalStaffType Type { get; set; }
        string Specialty { get; set; }
        string Location { get; set; } // Room
        string Shift { get; set; }
        string WorkingHours { get; set; }
        string DepartmentId { get; set; }
        HDepartment Department { get; set; }

        PatientQueue PatientQueue { get; set; }
        // Lab Lab { get; set; }
        // int LabId { get; set; }

        ICollection<SurgeryRecord> SurgeryRecords { get; set; }

    }

    public class HospitalStaff : Person, IHospitalStaff {
        //doctor
        public HospitalStaffType Type { get; set; }

        //dentist or surgent
        public string Specialty { get; set; }
        public string Shift { get; set; }
        public string Location { get; set; }
        public string WorkingHours { get; set; }
        public string DepartmentId { get; set; }
        public HDepartment Department { get; set; }

        //public Lab Lab { get; set; }
        //  public int LabId { get; set; }

        public PatientQueue PatientQueue { get; set; }
        public ICollection<SurgeryRecord> SurgeryRecords { get; set; }

        public string FullName {
            get{
                return FirstName+" "+MiddleName+" "+LastName;
            }
        
        }
        public bool IsEqual(Person obj){
            if(obj.FirstName.ToLower().Equals(FirstName.ToLower())&&
            obj.LastName.ToLower().Equals(LastName.ToLower())&&
            obj.MiddleName.ToLower().Equals(MiddleName.ToLower())&&
            obj.BiometricScan.ToLower().Equals(BiometricScan.ToLower())&&
            obj.DateOfBirth.ToLower().Equals(DateOfBirth.ToLower())){
                return true;
            }
            return false;
        }
        public bool IsValid () {
            var gender = "";
            var maritalStatus = "";
            Enum.Parse<Gender>(gender);
            Enum.Parse<MaritalStatus>(maritalStatus);

                if (string.IsNullOrEmpty (BiometricScan) ||
                    string.IsNullOrEmpty (BloodGroup) ||
                    !Contact.IsValid() ||
                    string.IsNullOrEmpty (DateOfBirth) ||
                    string.IsNullOrEmpty (DepartmentId) ||
                    string.IsNullOrEmpty (LastName) ||
                    string.IsNullOrEmpty (FirstName) ||
                    string.IsNullOrEmpty (gender)||
                    string.IsNullOrEmpty (Title) ||
                    string.IsNullOrEmpty (maritalStatus) ||
                    string.IsNullOrEmpty (Specialty) ||
                    string.IsNullOrEmpty (Shift) ||
                    string.IsNullOrEmpty (Location) ||
                    string.IsNullOrEmpty (WorkingHours) ||
                    string.IsNullOrEmpty (DepartmentId)
                ) return false;

                return true;
            }
        }

    }