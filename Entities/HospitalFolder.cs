using System;

namespace Entities {
    public class HospitalFolder {
        public HospitalFolder(Hospital hospital, Folder folder){
            Hospital = hospital;
            Folder = folder;
        }
        public string HospitalId { get; set; }
        public Hospital Hospital { get; set; }
        public string FolderId { get; set; }
        public Folder Folder { get; set; }

    }
}