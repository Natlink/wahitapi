﻿namespace Entities {
    public interface ILabData {
        int LabDataId { get; set; }
        string Note { get; set; }
        string TestName { get; set; }
        string TestResult { get; set; }
        string DateRecorded { get; set; }
        string LastModified { get; set; }
        Lab Lab { get; set; }
        Folder Folder { get; set; }
        HospitalStaff Technician { get; set; }

        bool IsValid();
        bool Equals(LabData data);
    }

    public class LabData : ILabData {
        private HospitalStaff _technician;
        public LabData () {
            DateRecorded = System.DateTime.Now.ToString ();
            LastModified = System.DateTime.Now.ToString ();
        }
        public int LabDataId { get; set; }
        public string Note { get; set; }
        public string TestName { get; set; }
        public string TestResult { get; set; }
        public string DateRecorded { get; set; }
        public string LastModified { get; set; }
        public virtual HospitalStaff Technician {
            get {
                return _technician;
            }
            set {
                if (value.Type == HospitalStaffType.LabTechnician) {
                    _technician = value;
                }
            }
        }
        public int LabId { get; set; }
        public Lab Lab { get; set; }
        public string FolderId { get; set; }
        public Folder Folder { get; set; }

        public bool IsValid(){
            if(string.IsNullOrEmpty(Note)||
            string.IsNullOrEmpty(TestName)||
            string.IsNullOrEmpty(TestResult)||
            string.IsNullOrEmpty(DateRecorded))
            return false;

            return true;
        }

        public  bool Equals(LabData obj){
            if(string.Equals(Note, obj.Note)&&
            string.Equals(TestName,obj.TestName)&&
            string.Equals(TestResult,obj.TestResult)&&
            string.Equals(DateRecorded,obj.DateRecorded)){
                return true;
            }
            return false;
        }
    }
}