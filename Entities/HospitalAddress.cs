﻿using Commons;

namespace Entities {
    public interface IHospitalAddress {
        string HospitalAddressId { get; set; }
        string Country { get; set; }
        string State { get; set; }
        string City { get; set; }
        string Street { get; set; }
        Hospital Hospital { get; set; }
    }
    public class HospitalAddress : NotificationEntity, IHospitalAddress {
        private string _hospitalAddressId;
        private string _country;
        private string _state;
        private string _city;
        private string _street;

        public string HospitalAddressId {
            get { return _hospitalAddressId; }
            set { SetWithNotify (value, ref _hospitalAddressId); }
        }
        public string Country {
            get { return _country; }
            set { SetWithNotify (value, ref _country); }
        }
        public string State {
            get { return _state; }
            set { SetWithNotify (value, ref _state); }
        }
        public string City {
            get { return _city; }
            set { SetWithNotify (value, ref _city); }
        }
        public string Street {
            get { return _street; }
            set { SetWithNotify (value, ref _street); }
        }

        public string HospitalRef { get; set; }
        public virtual Hospital Hospital { get; set; }

    }
}