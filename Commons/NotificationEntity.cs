using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Commons {
    public class NotificationEntity : INotifyPropertyChanged, INotifyPropertyChanging {
        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        protected void SetWithNotify<T> (T value, ref T field, [CallerMemberName] string propertyName = "") {
            if (!Equals (field, value)) {
                PropertyChanging?.Invoke (this, new PropertyChangingEventArgs (propertyName));
                field = value;
                PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
            }
        }
    }
}