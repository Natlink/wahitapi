﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataService;
using Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WahitBackend.Models;

namespace WahitBackend.Controllers {
    [Route ("wahitapi/[controller]")]
    [EnableCors ("AllowAngularConnectLocal")]
    public class LabController : Controller {
        private ILabService _labService;
        private readonly IMapper _mapper;
        private readonly ILogger<LabController> _logger;
        public LabController (ILabService labService,
            IMapper mapper, ILogger<LabController> logger) {
            _labService = labService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet ("alldepartmentlabs/{departmentId}")]
        public async Task<IActionResult> AllDepartmentLabs (string departmentId) //Actions
        {
            try {
                _logger.LogInformation ("Request to get all labs  in information");
                var labModel = new List<LabModel> ();
                foreach (var lab in await _labService.GetLabsByDepartment (departmentId)) {
                    labModel.Add (_mapper.Map<LabModel> (lab));
                }
                return Ok (labModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching Hospials from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("alllabs")]
        public async Task<IActionResult> AllLabs () //Actions
        {
            try {
                _logger.LogInformation ("Request to get all labs  in information");
                var labModel = new List<LabModel> ();
                foreach (var lab in await _labService.GetAllLabs ()) {
                    labModel.Add (_mapper.Map<LabModel> (lab));
                }
                return Ok (labModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching labs from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("getlabbyid/{labId}/{deptId}")]
        public async Task<IActionResult> GetLabById (int labId, string deptId) //Actions
        {
            try {
                _logger.LogInformation ("Looking for lab with Id: ${id}");
                if (labId<0 || string.IsNullOrEmpty (deptId)) {
                    _logger.LogError ($"Please specify the lab and department IDs in query");

                    return Json (new { status = "false", msg = "Lab Not Found" });
                }
                var dpt = await _labService.GetLabById (labId, deptId);
                return Ok (_mapper.Map<LabModel> (dpt));
            } catch (Exception e) {
                _logger.LogError (e, $"Lookup error occurred finding lab with ID: {labId} of department with ID: {deptId}");
            }
            return Json (new { status = "false", msg = "Lab Not Found" });
        }

        [HttpPost ("add")]
        public async Task<IActionResult> Add ([FromBody] LabModel model) {
            try {
                _logger.LogInformation ("Add new lab to database");
                if (ModelState.IsValid) {
                    var lab = _mapper.Map<Lab> (model);

                    await _labService.CreateLab (lab);
                    _logger.LogError ($"{model.name} has been created successfully");
                    return Ok (Json (new { status = "true", msg = $"{model.name} has been created successfully" }));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Add {model.name} lab failed");
            }

            return Ok (Json (new { status = "false", msg = "Error occurred adding lab, please try again later" }));
        }

        [HttpPost ("update")]
        public async Task<IActionResult> Update ([FromBody] LabModel model) {
            try {
                _logger.LogInformation ($"Updating lab with ID: {model.labId} to database");
                if (ModelState.IsValid) {
                    var lab = _mapper.Map<Lab> (model);

                    if (await _labService.UpdateLab (lab)) {
                        _logger.LogError ($"{model.name} has been updated successfully");
                        return Ok (Json (new { status = "true", msg = $"{model.name} has been updated successfully" }));
                    }
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Updating {model.name} lab failed");
            }
            _logger.LogError ($"Updating {model.name} lab of department with ID: {model.departmentId} failed ");
            return Ok (Json (new { status = "false", msg = $"Error occurred updating{model.name} lab, please try again later" }));

        }

        [HttpPost ("delete/{labIds}/{deptId}")]
        public async Task<IActionResult> Delete (string labIds, string deptId) {
            try {
                if (string.IsNullOrEmpty (labIds) || string.IsNullOrEmpty (deptId)) {
                    _logger.LogError ($"Lab and hospital IDs, must be set for the delete query");
                    return Json (new { status = "false", msg = "Please select lab to delete" });
                }

                _logger.LogInformation ($"Deleting labs with ID: {labIds} of department with ID: {deptId}");
                var tempIds = labIds.Split (',');
                if (ModelState.IsValid) {
                    bool isDeleted = true;
                    foreach (var id in tempIds) {
                        try{
                        if (!await _labService.DeleteLab (int.Parse(id), deptId)) {
                            isDeleted = false;
                        }}catch(Exception){}
                    }
                    if (isDeleted) {
                        return Json (new { status = "true", msg = "Selected labs have been successfully deleted" });
                    }

                }
            } catch (Exception e) {
                _logger.LogError (e, $"deleting lab(s) of ID(s) {labIds} failed");
            }
            return Json (new { status = "false", msg = "Unknown errors occurred deleting the labs" });
        }

    }
}