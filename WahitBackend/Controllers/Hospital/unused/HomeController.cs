﻿// using System.Web.Mvc;

// namespace WAHIT.Controllers
// {
//     public class HomeController : Controller
//     {
//         [AllowAnonymous]
//         public ActionResult Index()
//         {
//             return File("~/Views/Pages/index.html", "text/html");
//         }

//         [AllowAnonymous]
//         public ActionResult Start()
//         {
//             return File("~/Views/Pages/start.html", "text/html");
//         }

//         public ActionResult Error404()
//         {
//             //Response.StatusCode = 404;
//             return File("~/Views/Pages/404.html", "text/html");
//         }
//         public ActionResult Error500()
//         {
//             //Response.StatusCode = 500;
//             return File("~/Views/Pages/500.html", "text/html");
//         }
//         public ActionResult Calendar()
//         {
//             return File("~/Views/Pages/calendar.html", "text/html");
//         }
//         public ActionResult DataTables()
//         {
//             return File("~/Views/Pages/datatables.html", "text/html");
//         }
//         public ActionResult PatientInfo()
//         {
//             return File("~/Views/Pages/patientinfo.html", "text/html");
//         }
//         public ActionResult Email()
//         {
//             return File("~/Views/Pages/email.html", "text/html");
//         }
//         public ActionResult EmailCompose()
//         {
//             return File("~/Views/Pages/email_compose.html", "text/html");
//         }
//         public ActionResult FileCreation()
//         {
//             return File("~/Views/Pages/file_creation.html", "text/html");
//         }
//         public ActionResult Invoice()
//         {
//             return File("~/Views/Pages/invoice.html", "text/html");
//         }
//         public ActionResult LabDetails()
//         {
//             return File("~/Views/lab_details.html", "text/html");
//         }
       
//         public ActionResult NurseView()
//         {
//             return File("~/Views/Pages/nurse_view.html", "text/html");
//         }
//         public ActionResult PatientQueue()
//         {
//             return File("~/Views/Pages/patient_queue.html", "text/html");
//         }
//         public ActionResult PatienDetails()
//         {
//             return File("~/Views/Pages/patient_viewdetails.html", "text/html");
//         }
//         public ActionResult PharmacyView()
//         {
//             return File("~/Views/Pages/pharmacy.html", "text/html");
//         }
//         public ActionResult UserProfile()
//         {
//             return File("~/Views/Pages/profile.html", "text/html");
//         }
//         public ActionResult Timeline()
//         {
//             return File("~/Views/Pages/timeline.html", "text/html");
//         }
//         public ActionResult Map()
//         {
//             return File("~/Views/Pages/map.html", "text/html");
//         }

//         public ActionResult News()
//         {
//             return File("~/Views/Pages/news.html", "text/html");
//         }

//         //Reporting / ADMIN VIEWS
//         public ActionResult Admin()
//         {
//             return File("~/Views/Pages/admin/dashboard.html", "text/html");
//         }
//         public ActionResult CreateHospital()
//         {
//             return File("~/Views/Pages/admin/create_update_hospital.html", "text/html");
//         }
//         public ActionResult CreateDepartment()
//         {
//             return File("~/Views/Pages/admin/create_update_dept.html", "text/html");
//         }
//         public ActionResult CreateStaff()
//         {
//             return File("~/Views/Pages/admin/create_update_staff.html", "text/html");
//         }
//         public ActionResult CreateWard()
//         {
//             return File("~/Views/Pages/admin/create_update_ward.html", "text/html");
//         }
//         public ActionResult DepartmentInfo()
//         {
//             return File("~/Views/Pages/admin/info_department.html", "text/html");
//         }
//         public ActionResult StaffInfo()
//         {
//             return File("~/Views/Pages/admin/info_staff.html", "text/html");
//         }
//         public ActionResult WardInfo()
//         {
//             return File("~/Views/Pages/admin/info_ward.html", "text/html");
//         }
//         public ActionResult MyHospital()
//         {
//             return File("~/Views/Pages/admin/myhospital.html", "text/html");
//         }



//     }
// }