﻿// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;
// using System.Threading.Tasks;
// using System.Web.Mvc;
// using AutoMapper;
// using BusinessLogic;
// using Ninject;
// using ServiceLayer;
// using WAHIT.Models;
// using WebGrease.Css.Extensions;

// namespace WAHIT.Controllers
// {
//     public class PatientController : Controller
//     {
//         private IPatientService _patientService;
//         private IHospitalService _hospitalService;

//         IKernel _kernel = new StandardKernel();

//         public PatientController(IPatientService patientService, IHospitalService hospitalService)
//         {
            


//         }

//         //GET 
//         public async Task<ActionResult> GetAll(int howmany,bool reset=false)
//         {
//             var msg = "Patients loaded successfully";
//             var succeed = true;
//             var patients = await Task.Run(() => _patientService.GetPatients(howmany, reset));

//             if (patients == null)
//             {
//                 msg = "No data found in database or system error occured";
//                 succeed = false;
//             }

//             var patientDtos = new PatientViewModel();
//             patients.ForEach(patient =>
//             {
//                 patientDtos = Mapper.Map<PatientViewModel>(patient);
//             });


//             //fetch all patients from the database
//             //returning json data of all patients
//             return Json(new
//             {
//                 patients = patientDtos,
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }

//         public async Task<ActionResult> GetPatient(string id)
//         {
//             var msg = "Patient fetched successfully";
//             var succeed = true;
//             var patient = await Task.Run(() => _patientService.GetPatientById(id));

//             if (patient == null)
//             {
//                 msg = "No data found in database for patient or system error occured";
//                 succeed = false;
//             }

//             var patientDto = Mapper.Map<PatientViewModel>(patient);

//             //
//             //returning a single patient object
//             return Json(new
//             {
//                 patient = patientDto,
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }
//         public async Task<ActionResult> SaveUpdatePatient(PatientViewModel model)
//         {
//             var msg = "Information successfully saved";
//             var succeed = true;

//             if (!ModelState.IsValid) return Json(new { Suceeded = false, msg = "Invalid patient information" });

//             var patient = Mapper.Map<Patient>(model);
//             if (patient == null)
//             {
//                 msg = "Saving information failed";
//                 succeed = false;
//             }
//             //Get hospital the patient has to be registered to 
      
//             var hospital = await Task.Run(() => _hospitalService.GetHospital(model.HospitalId));

//             //perform new insertion or update
//             await Task.Run(() => _patientService.AddOrUpdatePatient(patient, hospital));

//             return Json(new
//             {
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }

//         public async Task<ActionResult> SaveUpdatePatients(List<PatientViewModel> model)
//         {
//             var msg = "Information successfully saved";
//             var succeed = true;

//             if (!ModelState.IsValid) return Json(new { Suceeded = false, msg = "Invalid patients information" });

//             var patients = new List<Patient>();

//             model.ForEach(patientvm =>
//             {
//                 patients.Add(Mapper.Map<Patient>(patientvm));
//             });

//             if (patients.Count <= 0)
//             {
//                 msg = "Saving information failed";
//                 succeed = false;
//             }

//             var hospitals =new List<IHospital>();
//             foreach (var patient in model)
//             {
//             hospitals.Add(await Task.Run(() => _hospitalService.GetHospital(patient.HospitalId))); 
//             }

//             //perform new insertion or update
            
//             for (var i = 0; i < hospitals.Count; i++)
//             {
//                 await Task.Run(() => _patientService.AddOrUpdatePatient(patients[i],hospitals[i]));
//             }

//             return Json(new
//             {
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }

//         public async Task<ActionResult> Delete(string id,string hospitalId)
//         {
//             var msg = "Deletion successful";
//             var succeed = true;

//             if (!ModelState.IsValid) return Json(new { Suceeded = false, msg = "Delete request failed" });

//             //perform new insertion or update
//             var patient = new Patient {Id = id};
//             var hospital = new Hospital {HospitalId = hospitalId};
//             if (!await Task.Run(() => _patientService.DeletePatient(patient, hospital)))
//             {
//                 succeed = false;
//                 msg = "Delete request failed";
//             }
//             return Json(new
//             {
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }

//         //PatientQueue
//         public async Task<ActionResult> GetPatientQueue()
//         {
//             var msg = "Patients in queue loaded successfully";
//             var succeed = true;
//             var patients = await Task.Run(() => _patientService.GetPatientQueue().ToList());

//             if (patients == null)
//             {
//                 msg = "No patient is in queue or system error occured";
//                 succeed = false;
//             }

//             var patientDtos = new List<PatientQueueViewModel>();

//             foreach (var patient in patients)
//             {
//                 patientDtos.Add(Mapper.Map<PatientQueueViewModel>(patient));
//             }

//             //
//             //returning a single patient object
//             return Json(new
//             {
//                 patients = patientDtos,
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }
//         public async Task<ActionResult> DeleteFromQueue(string id)
//         {
//             var msg = "Deletion successful";
//             var succeed = true;

//             if (!ModelState.IsValid) return Json(new { Suceeded = false, msg = "Deleting from queue failed" });
           
//             if (!await Task.Run(() => _patientService.DeleteFromPatientQueue(id)))
//             {
//                 succeed = false;
//                 msg = "Deleting from queue failed";
//             }
//             return Json(new
//             {
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }
//         public async Task<ActionResult> AddPatientToQueue(PatientQueueViewModel model)
//         {
//             var msg = "Patient is added to queue";
//             var succeed = true;

//             if (!ModelState.IsValid) return Json(new { Suceeded = false, msg = "Invalid patient information" });

//             var patient = Mapper.Map<PatientQueue>(model);
//             if (patient == null)
//             {
//                 msg = "Error occured saving patient to queue";
//                 succeed = false;
//             }

//             //perform new insertion or update
//             await Task.Run(() => _patientService.AddPatientToQueue(patient));
//             return Json(new
//             {
//                 msg = msg,
//                 Succeeded = succeed
//             });
//         }


//     }
// }