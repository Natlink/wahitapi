﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataService;
using Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WahitBackend.Models;

namespace WahitBackend.Controllers {
    [Route ("wahitapi/[controller]")]
    [EnableCors ("AllowAngularConnectLocal")]
    public class DepartmentController : Controller {
        private IHDepartmentService _departmentService;
        private readonly IMapper _mapper;
        private readonly ILogger<DepartmentController> _logger;
        public DepartmentController (IHDepartmentService departmentService,
            IMapper mapper, ILogger<DepartmentController> logger) {
            _departmentService = departmentService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet ("all/{hospitalId}")]
        public async Task<IActionResult> All (string hospitalId) //Actions
        {
            try {
                _logger.LogInformation ("Request to get all departments  in information");
                var departmentModel = new List<HospitalDepartmentModel> ();
                foreach (var department in await _departmentService.GetHDepartments (hospitalId)) {
                    departmentModel.Add (_mapper.Map<HospitalDepartmentModel> (department));
                }
                return Ok (departmentModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching Hospials from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("getdepartmentbyid/{deptId}/{hosId}")]
        public async Task<IActionResult> GetDepartmentById (string deptId, string hosId) //Actions
        {
            try {
                _logger.LogInformation ("Looking for department with Id: ${id}");
                if (string.IsNullOrEmpty (deptId) || string.IsNullOrEmpty (hosId)) {
                    _logger.LogError ($"Please specify the department and hospital IDs in query");

                    return Json (new { status = "false", msg = "Department Not Found" });
                }
                var dpt = await _departmentService.GetHDepartmentById (deptId, hosId);
                return Ok (_mapper.Map<HospitalDepartmentModel> (dpt));
            } catch (Exception e) {
                _logger.LogError (e, $"Lookup error occurred finding department with ID: {deptId} of hospital with ID: {hosId}");
            }
            return Json (new { status = "false", msg = "Department Not Found" });
        }

        [HttpPost ("add")]
        public async Task<IActionResult> Add ([FromBody] HospitalDepartmentModel model) {
            try {
                _logger.LogInformation ("Add new department to database");
                if (ModelState.IsValid) {
                    var department = _mapper.Map<HDepartment> (model);

                    await _departmentService.CreateHospitalDepartment (model.hospitalId, department);
                    _logger.LogError ($"{model.name} has been created successfully");
                    return Ok (Json (new { status = "true", msg = $"{model.name} has been created successfully" }));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Add department({model.name}) failed");
            }

            return Ok (Json (new { status = "false", msg = "Error occurred adding department, please try again later" }));
        }

        [HttpPost ("update")]
        public async Task<IActionResult> Update ([FromBody] HospitalDepartmentModel model) {
            try {
                _logger.LogInformation ($"Updating department with ID: {model.departmentId} to database");
                if (ModelState.IsValid) {
                    var department = _mapper.Map<HDepartment> (model);

                    if (await _departmentService.UpdateHospitalDepartment (department)) {
                        _logger.LogError ($"{model.name} has been updated successfully");
                        return Ok (Json (new { status = "true", msg = $"{model.name} has been updated successfully" }));
                    }
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Updating {model.name} department failed");
            }
            _logger.LogError ($"Updating {model.name} department of hospital with ID: {model.hospitalId} failed ");
            return Ok (Json (new { status = "false", msg = $"Error occurred updating{model.name} department, please try again later" }));

        }

        [HttpPost ("delete/{deptIds}/{hosId}")]
        public async Task<IActionResult> Delete (string deptIds, string hosId) {
            try {
                if (string.IsNullOrEmpty (deptIds) || string.IsNullOrEmpty (hosId)) {
                    _logger.LogError ($"Department and hospital IDs, must be set for the delete query");
                    return Json (new { status = "false", msg = "Please select department to delete" });
                }

                _logger.LogInformation ($"Deleting departments with ID: {deptIds} of hospital with ID: {hosId}");
                var tempIds = deptIds.Split (',');
                if (ModelState.IsValid) {
                    bool isDeleted = true;
                    foreach (var id in tempIds) {
                        if (!await _departmentService.DeleteHospitalDepartment (hosId, id)) {
                            isDeleted = false;
                        }
                    }
                    if (isDeleted) {
                        return Json (new { status = "true", msg = "Selected departments have been successfully deleted" });
                    }

                }
            } catch (Exception e) {
                _logger.LogError (e, $"deleting department(s) of ID(s) {deptIds} failed");
            }
            return Json (new { status = "false", msg = "Unknown errors occurred deleting the departments" });
        }

    }
}