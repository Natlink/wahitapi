﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataService;
using Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WahitBackend.Models;

namespace WahitBackend.Controllers {
    [Route ("wahitapi/[controller]")]
    [EnableCors ("AllowAngularConnectLocal")]
    public class HospitalController : Controller {
        private IHospitalService _hospitalService;
        private readonly IMapper _mapper;
        private readonly ILogger<HospitalController> _logger;
        public HospitalController (IHospitalService hospitalService,
            IMapper mapper, ILogger<HospitalController> logger) {
            _hospitalService = hospitalService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet ("all")]
        public async Task<IActionResult> All () //Actions
        {
            try {
                _logger.LogInformation ("Request to get all Hospitals in information");
                var hospitalModel = new List<HospitalModel> ();
                foreach (var hos in await _hospitalService.GetHospitals ()) {
                    hospitalModel.Add (_mapper.Map<HospitalModel> (hos));
                }
                return Ok (hospitalModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching Hospials from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("gethospitalbyid/{id}")]
        public async Task<IActionResult> GetHospitalById (string id) //Actions
        {
            try {
                _logger.LogInformation ("Looking for hospital with Id: ${id}");
                if (string.IsNullOrEmpty (id)) {
                    return new EmptyResult ();
                }
                var hos = await _hospitalService.GetHospitalById (id);
                return Ok (_mapper.Map<HospitalModel> (hos));
            } catch (Exception e) {
                _logger.LogError (e, $"Lookup error occured finding hospital with ID: {id}");
            }
            return Ok (new EmptyResult ());
        }

        [HttpPost ("gethospitalbyaddress")]
        public async Task<IActionResult> GetHospitalByAddress ([FromBody] HospitalAddressModel model) {
            try {
                _logger.LogInformation ($"Looking for hospital by address: {model.ToString()}");

                if (ModelState.IsValid) {
                    var address = _mapper.Map<HospitalAddress> (model);
                    return Ok (_mapper.Map<HospitalModel> (await _hospitalService.GetHospitalByAddress (address)));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Lookup error occurred finding hospital with ID: {model.ToString()}");

            }
            return Ok (new { msg = "No hospital found" });
        }

        [HttpPost ("add")]
        public async Task<IActionResult> Add ([FromBody] HospitalModel model) {
            try {
                _logger.LogInformation ("Add new hospital to database");
                if (ModelState.IsValid) {
                    var hospital = _mapper.Map<Hospital> (model);
                    return Ok (await _hospitalService.AddHospital (hospital));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Add hospital({model.name}) failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpPost ("update")]
        public async Task<IActionResult> Update ([FromBody] HospitalModel model) {
            try {
                _logger.LogInformation ($"Updating hospital with ID: {model.id} to database");
                if (ModelState.IsValid) {
                    var hospital = _mapper.Map<Hospital> (model);
                    return Ok (await _hospitalService.UpdateHospital (hospital));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Updating hospital({model.name}) failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpPost ("delete/{ids}")]
        public async Task<IActionResult> Delete (string ids) {
            try {
                _logger.LogInformation ($"Deleting hospitals with ID: {ids}");
                var tempIds = ids.Split (',');
                if (ModelState.IsValid) {
                    foreach (var id in tempIds) {
                        await _hospitalService.DeleteHospitalById (id);
                    }
                    return Ok (true);
                }
            } catch (Exception e) {
                _logger.LogError (e, $"deleting hospital(s) of ID(s) {ids} failed");
            }
            return Ok (new EmptyResult ());
        }

    }
}