﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataService;
using Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WahitBackend.Models;

namespace WahitBackend.Controllers {
    [Route ("wahitapi/[controller]")]
    [EnableCors ("AllowAngularConnectLocal")]
    public class WardController : Controller {
        private IWardService _wardService;
        private readonly IMapper _mapper;
        private readonly ILogger<WardController> _logger;
        public WardController (IWardService wardService,
            IMapper mapper, ILogger<WardController> logger) {
            _wardService = wardService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet ("alldepartmentwards/{departmentId}")]
        public async Task<IActionResult> AllDepartmentWards (string departmentId) //Actions
        {
            try {
                _logger.LogInformation ("Request to get all wards  in information");
                var wardModel = new List<WardModel> ();
                foreach (var ward in await _wardService.GetWardsByDepartment (departmentId)) {
                    wardModel.Add (_mapper.Map<WardModel> (ward));
                }
                return Ok (wardModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching Hospials from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("allwards")]
        public async Task<IActionResult> AllWards () //Actions
        {
            try {
                _logger.LogInformation ("Request to get all wards  in information");
                var wardModel = new List<WardModel> ();
                foreach (var ward in await _wardService.GetAllWards ()) {
                    wardModel.Add (_mapper.Map<WardModel> (ward));
                }
                return Ok (wardModel);
            } catch (Exception e) {
                _logger.LogError (e, "Fetching wards from database failed");
            }
            return Ok (new EmptyResult ());
        }

        [HttpGet ("getwardbyid/{wardId}/{deptId}")]
        public async Task<IActionResult> GetWardById (string wardId, string deptId) //Actions
        {
            try {
                _logger.LogInformation ("Looking for ward with Id: ${id}");
                if (string.IsNullOrEmpty (wardId) || string.IsNullOrEmpty (deptId)) {
                    _logger.LogError ($"Please specify the ward and department IDs in query");

                    return Json (new { status = "false", msg = "Ward Not Found" });
                }
                var dpt = await _wardService.GetWardById (wardId, deptId);
                return Ok (_mapper.Map<WardModel> (dpt));
            } catch (Exception e) {
                _logger.LogError (e, $"Lookup error occurred finding ward with ID: {wardId} of department with ID: {deptId}");
            }
            return Json (new { status = "false", msg = "Ward Not Found" });
        }

        [HttpPost ("add")]
        public async Task<IActionResult> Add ([FromBody] WardModel model) {
            try {
                _logger.LogInformation ("Add new ward to database");
                if (ModelState.IsValid) {
                    var ward = _mapper.Map<Ward> (model);

                    await _wardService.CreateWard (ward);
                    _logger.LogError ($"{model.name} has been created successfully");
                    return Ok (Json (new { status = "true", msg = $"{model.name} has been created successfully" }));
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Add {model.name} ward failed");
            }

            return Ok (Json (new { status = "false", msg = "Error occurred adding ward, please try again later" }));
        }

        [HttpPost ("update")]
        public async Task<IActionResult> Update ([FromBody] WardModel model) {
            try {
                _logger.LogInformation ($"Updating ward with ID: {model.wardId} to database");
                if (ModelState.IsValid) {
                    var ward = _mapper.Map<Ward> (model);

                    if (await _wardService.UpdateWard (ward)) {
                        _logger.LogError ($"{model.name} has been updated successfully");
                        return Ok (Json (new { status = "true", msg = $"{model.name} has been updated successfully" }));
                    }
                }
            } catch (Exception e) {
                _logger.LogError (e, $"Updating {model.name} ward failed");
            }
            _logger.LogError ($"Updating {model.name} ward of hospital with ID: {model.hospitalId} failed ");
            return Ok (Json (new { status = "false", msg = $"Error occurred updating{model.name} ward, please try again later" }));

        }

        [HttpPost ("delete/{wardIds}/{deptId}")]
        public async Task<IActionResult> Delete (string wardIds, string deptId) {
            try {
                if (string.IsNullOrEmpty (wardIds) || string.IsNullOrEmpty (deptId)) {
                    _logger.LogError ($"Ward and hospital IDs, must be set for the delete query");
                    return Json (new { status = "false", msg = "Please select ward to delete" });
                }

                _logger.LogInformation ($"Deleting wards with ID: {wardIds} of department with ID: {deptId}");
                var tempIds = wardIds.Split (',');
                if (ModelState.IsValid) {
                    bool isDeleted = true;
                    foreach (var id in tempIds) {
                        if (!await _wardService.DeleteWard (id, deptId)) {
                            isDeleted = false;
                        }
                    }
                    if (isDeleted) {
                        return Json (new { status = "true", msg = "Selected wards have been successfully deleted" });
                    }

                }
            } catch (Exception e) {
                _logger.LogError (e, $"deleting ward(s) of ID(s) {wardIds} failed");
            }
            return Json (new { status = "false", msg = "Unknown errors occurred deleting the wards" });
        }

    }
}