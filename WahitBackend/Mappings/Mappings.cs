﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Commons;
using Entities;
using WahitBackend.Models;

namespace WahitBackend {
    public class MappingProfile : Profile {
        public MappingProfile () {
            #region
            //Creating mappings
            CreateMap<Hospital, HospitalModel> ()
                .ForMember (h => h.id, opt => opt.MapFrom (src => src.HospitalId))
                .ForMember (h => h.about, opt => opt.MapFrom (src => src.About))
                .ForMember (h => h.imgUrls, opt => opt.MapFrom (src => src.Photos))
                .ForMember (h => h.name, opt => opt.MapFrom (src => src.Name))
                .ForMember (h => h.workingHours, opt => opt.MapFrom (src => src.WorkingHours))
                .ReverseMap ();

            #endregion

            CreateMap<Map, MapModel> ().ReverseMap ();
            CreateMap<HospitalAddress, HospitalAddressModel> ()
                .ForMember (a => a.town, opt => opt.MapFrom (src => src.City))
                .ReverseMap ();

            CreateMap<HDepartment, HospitalDepartmentModel> ()
                .ForMember (h => h.departmentId, opt => opt.MapFrom (src => src.HDepartmentId))
                .ForMember (h => h.asshod, opt => opt.MapFrom (src => src.AssistantHOD))
                .ForMember (h => h.hod, opt => opt.MapFrom (src => src.HOD))
                .ForMember (h => h.name, opt => opt.MapFrom (src => src.Name))
                .ForMember (h => h.hospitalId, opt => opt.MapFrom (src => src.HospitalId))
                .ReverseMap ();

            CreateMap<Person, PersonModel> ()
                .ForMember (h => h.biometricScan, opt => opt.MapFrom (src => src.BiometricScan))
                .ForMember (h => h.bloodGroup, opt => opt.MapFrom (src => src.BloodGroup))
                .ForMember (h => h.dateOfBirth, opt => opt.MapFrom (src => src.DateOfBirth))
                .ForMember (h => h.firstName, opt => opt.MapFrom (src => src.FirstName))
                .ForMember (h => h.gender, opt => opt.MapFrom (src => src.Gender))
                .ForMember (h => h.id, opt => opt.MapFrom (src => src.PersonId))
                .ForMember (h => h.lastName, opt => opt.MapFrom (src => src.LastName))
                .ForMember (h => h.maritalStatus, opt => opt.MapFrom (src => src.MaritalStatus))
                .ForMember (h => h.middleName, opt => opt.MapFrom (src => src.MiddleName))
                .ForMember (h => h.photos, opt => opt.MapFrom (src => src.Photos))
                .ForMember (h => h.title, opt => opt.MapFrom (src => src.Title))
                .ReverseMap ();

            CreateMap<Patient, PatientModel> ()
                .ForMember (p => p.bedNumber, opt => opt.MapFrom (src => src.BedNumber))
                .ForMember (p => p.dateAdmitted, opt => opt.MapFrom (src => src.DateAdmitted))
                .ForMember (p => p.dateDischarged, opt => opt.MapFrom (src => src.DateDischarged))
                .ForMember (p => p.queueId, opt => opt.MapFrom (src => src.PatientQueue.Id))
                .ForMember (p => p.roomNumber, opt => opt.MapFrom (src => src.RoomNumber))
                .IncludeBase<Person, PersonModel> ().ReverseMap ();

            CreateMap<HospitalStaff, StaffModel> ()
                .ForMember (p => p.departmentId, opt => opt.MapFrom (src => src.DepartmentId))
                .ForMember (p => p.workingHours, opt => opt.MapFrom (src => src.WorkingHours))
                .ForMember (p => p.type, opt => opt.MapFrom (src => src.Type))
                .ForMember (p => p.specialty, opt => opt.MapFrom (src => src.Specialty))
                .ForMember (p => p.shift, opt => opt.MapFrom (src => src.Shift))
                .IncludeBase<Person, PersonModel> ().ReverseMap ();

            CreateMap<Contact, ContactModel> ()
                .ForMember (p => p.city, opt => opt.MapFrom (src => src.City))
                .ForMember (p => p.country, opt => opt.MapFrom (src => src.Country))
                .ForMember (p => p.email, opt => opt.MapFrom (src => src.Email))
                .ForMember (p => p.homePhone, opt => opt.MapFrom (src => src.HomePhone))
                .ForMember (p => p.houseAddress, opt => opt.MapFrom (src => src.HouseAddress))
                .ForMember (p => p.mobilePhone, opt => opt.MapFrom (src => src.MobilePhone))
                .ForMember (p => p.postalCode, opt => opt.MapFrom (src => src.PostalCode))
                .ForMember (p => p.state, opt => opt.MapFrom (src => src.State))
                .ForMember (p => p.street, opt => opt.MapFrom (src => src.Street))
                .ForMember (p => p.workPhone, opt => opt.MapFrom (src => src.WorkPhone))
                .ReverseMap ();

            CreateMap<Folder, FolderModel> ()
                .ForMember (p => p.patientId, opt => opt.MapFrom (src => src.PatientId))
                .ForMember (p => p.folderId, opt => opt.MapFrom (src => src.FolderId))
                .ReverseMap ();

            CreateMap<Ward, WardModel> ()
                .ForMember (w => w.wardId, opt => opt.MapFrom (src => src.WardId))
                .ForMember (w => w.departmentId, opt => opt.MapFrom (src => src.DepartmentId))
                .ForMember (w => w.type, opt => opt.MapFrom (src => src.Type))
                .ForMember (w => w.name, opt => opt.MapFrom (src => src.Name))
                .ReverseMap ();
            CreateMap<WardRoom, WardRoomModel> ()
                .ForMember (r => r.wardRoomId, opt => opt.MapFrom (src => src.WardRoomId))
                .ForMember (r => r.number, opt => opt.MapFrom (src => src.Number))
                .ForMember (r => r.wardId, opt => opt.MapFrom (src => src.Ward.WardId))
                .ReverseMap ();

            CreateMap<Bed, BedModel> ()
                .ForMember (b => b.bedId, opt => opt.MapFrom (src => src.BedId))
                .ForMember (b => b.number, opt => opt.MapFrom (src => src.Number))
                .ForMember (b => b.location, opt => opt.MapFrom (src => src.Location))
                .ForMember (b => b.patientId, opt => opt.MapFrom (src => src.PatientId))
                .ForMember (b => b.inUsed, opt => opt.MapFrom (src => string.IsNullOrEmpty (src.PatientId) ? true : false))
                .ForMember (b => b.roomId, opt => opt.MapFrom (src => src.RoomId))
                .ReverseMap ();

            CreateMap<Lab, LabModel> ()
                .ForMember (l => l.labId, opt => opt.MapFrom (src => src.LabId))
                .ForMember (l => l.name, opt => opt.MapFrom (src => src.Name))
                .ForMember (l => l.departmentId, opt => opt.MapFrom (src => src.DepartmentId))
                .ReverseMap ();

            CreateMap<SurgeryRecord, SurgeryRecordModel> ()
                .ForMember (r => r.surgeryRecordId, opt => opt.MapFrom (src => src.SurgeryRecordId))
                .ForMember (r => r.surgeonId, opt => opt.MapFrom (src => src.SurgeonId))
                .ForMember (r => r.dateRecorded, opt => opt.MapFrom (src => src.DateRecorded))
                .ForMember (r => r.lastModified, opt => opt.MapFrom (src => src.LastModified))
                .ForMember (r => r.theatreName, opt => opt.MapFrom (src => src.TheatreName))
                .ForMember (r => r.folderId, opt => opt.MapFrom (src => src.FolderId))
                .ForMember (r => r.location, opt => opt.MapFrom (src => src.Location))
                .ReverseMap ();

        }
    }
}