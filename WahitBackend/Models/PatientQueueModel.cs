namespace WahitBackend.Models {
    public class PatientQueueModel {
        public string id { get; set; }
        public string patientId { get; set; }
        public string doctorId { get; set; }
    }
}