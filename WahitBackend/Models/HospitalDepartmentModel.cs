using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Entities;

namespace WahitBackend.Models {
    public class HospitalDepartmentModel {
        public string departmentId { get; set; }
        public string asshod { get; set; }
        public string hod { get; set; }
        public string about{get; set;}
        [Required]
        [StringLength (100, ErrorMessage = "Please provide a valid Department name")]
        public string name { get; set; }

        [Required]
        [StringLength (100, ErrorMessage = "Invalid hospital Id")]
        public string hospitalId { get; set; }
    } 

    public class BedModel {
        public string bedId { get; set; }
        public string number { get; set; }
        public string location { get; set; }
        public bool inUsed { get; set; }
        public string roomId { get; set; }
        public string patientId{get; set;} 
    }

    public class WardModel {
        public string wardId { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string[] rooms { get; set; }
        public string departmentId { get; set; }
        public string hospitalId{get; set;}
    }

    public class WardRoomModel {
        public string wardRoomId { get; set; }
        public string number { get; set; }
        public string[] beds { get; set; }
        public string wardId { get; set; }
    }

    public class LabModel {
        public string labId { get; set; }
        public string name { get; set; }
        public string departmentId { get; set; } 
        public string[] staff{get;set;}    
        public string[] labData{get;set;}   
    }

}