using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Entities;

namespace WahitBackend.Models {
    public class HospitalModel {
        public string id { get; set; }
        [Required]
        [StringLength (50, ErrorMessage = "Please provide please the hospital requires a name")]
        public string name { get; set; }
        public string imgUrls { get; set; }
        [Required]
        [StringLength (50, ErrorMessage = "Please specify the working hours")]
        public string workingHours { get; set; }
        [Required]
        [StringLength (50, ErrorMessage = "Please provide information about the hospital")]
        public string about { get; set; }

        public MapModel map{get;set;}
        public HospitalAddressModel address{get;set;}
    }



    public class HospitalAddressModel{
        [Required]
        [StringLength (50, ErrorMessage = "Please provide a valid Country")]
        public string country { get; set; }
        [Required]
        [StringLength (100, ErrorMessage = "Please provide a valid State")]
        public string state { get; set; }
        [Required]
        [StringLength (100, ErrorMessage = "Please provide a valid Town")]
        public string town { get; set; }
        public string street { get; set; }
    }
}