using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Entities;

namespace WahitBackend.Models {
    
    public class PersonModel {
        public string id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string gender { get; set; }
        public string dateOfBirth { get; set; }
        public ContactModel contact { get; set; }
        public string title { get; set; }
        public string maritalStatus { get; set; }
        public string bloodGroup { get; set; }
        public string genotype { get; set; }
        public string biometricScan { get; set; }
        public string photos { get; set; }
    }

    
    public class PatientModel : PersonModel {
        //Patient
        public string bedNumber { get; set; }
        public string dateAdmitted { get; set; }
        public string dateDischarged { get; set; }
        public string roomNumber { get; set; }
        public string FolderId { get; set; }
        public virtual string queueId { get; set; }

    }

    
    public class StaffModel : PersonModel {
        public string type { get; set; }
        //dentist or surgent
        public string specialty { get; set; }
        public string shift { get; set; }
        public string workingHours { get; set; }
        public string departmentId { get; set; }

    }
}