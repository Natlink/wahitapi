namespace WahitBackend.Models {

    public class FolderModel {
        public string folderId { get; set; }
        public string patientId { get; set; }

    }

    public class SurgeryRecordModel {
        public string surgeryRecordId { get; set; }
        public string dateRecorded { get; set; }
        public string lastModified { get; set; }
        public string location { get; set; }
        public string theatreName { get; set; }
        public string surgeonId { get; set; }
        public string folderId { get; set; }
    }

    public class LabDataModel {
        public int labDataId { get; set; }
        public string note { get; set; }
        public string testName { get; set; }
        public string testResult { get; set; }
        public string dateRecorded { get; set; }
        public string lastModified { get; set; }
        public string technicianId { get; set; }
        public string labId { get; set; }
        public string folderId { get; set; }
    }

}