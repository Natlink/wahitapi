namespace WahitBackend.Models {
    public class ContactModel {
        public string country { get; set; }
        public string email { get; set; }
        public string mobilePhone { get; set; }
        public string workPhone { get; set; }
        public string homePhone { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string houseAddress { get; set; }
        public string postalCode { get; set; }
        public string street { get; set; }
    }
}