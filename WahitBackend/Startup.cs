﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataContext;
using DataService;
using Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WahitBackend.Data;
using WahitBackend.Models;
using WahitBackend.Services;

namespace WahitBackend {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            var config = new AutoMapper.MapperConfiguration (cfg => {
                cfg.AddProfile (new MappingProfile ());
            });

            var mapper = config.CreateMapper ();
            services.AddSingleton (mapper);

            services.AddDbContext<ApplicationDbContext> (options =>
                options.UseSqlServer (Configuration.GetConnectionString ("UserAccountConnection")));

            // Configuring for wahit Hospital database context
            var contextOptions = new DbContextOptionsBuilder<WahitHospitalContext> ()
                .UseSqlServer (Configuration.GetConnectionString ("DefaultConnection"))
                .Options;
            services.AddSingleton (contextOptions)
                .AddTransient<WahitHospitalContext> ()
                .AddTransient<IDbContext, WahitHospitalContext> ();

            //Configure for Wahit Management Database context

            //  Registering project services
            services
                //Register DBContext for entity
                .AddScoped<IDbContextFactory<WahitHospitalContext>, DbContextFactory<WahitHospitalContext>> ()
                .AddSingleton (typeof (IDbContextFactory<Hospital>), new DbContextFactory<Hospital> (services))
                .AddSingleton (typeof (IDbContextFactory<HDepartment>), new DbContextFactory<HDepartment> (services))
                .AddSingleton (typeof (IDbContextFactory<Lab>), new DbContextFactory<Lab> (services))
                .AddSingleton (typeof (IDbContextFactory<Ward>), new DbContextFactory<Ward> (services))
                .AddSingleton (typeof (IDbContextFactory<HospitalStaff>), new DbContextFactory<HospitalStaff> (services))

                //Register Repository for the entity
                .AddScoped<IRepo<Hospital>, Repo<Hospital>> ()
                .AddScoped<IRepo<HDepartment>, Repo<HDepartment>> ()
                .AddScoped<IRepo<Lab>, Repo<Lab>> ()
                .AddScoped<IRepo<Ward>, Repo<Ward>> ()
                .AddScoped<IRepo<HospitalStaff>, Repo<HospitalStaff>> ()

                //Register Services for the entity
                .AddScoped<IHospitalService, HospitalService> ()
                .AddScoped<IHDepartmentService, HDepartmentService> ()
                .AddScoped<ILabService, LabService> ()
                .AddScoped<IWardService, WardService> ()
                .AddScoped<IHospitalStaffService, HospitalStaffService> ();

            /************************************************************************************************
            NOTE: MODIFY THE IDBCONTEXTFACTORY IF DIFFERENT DBCONTEXT IS SUPPOSED TO BE USED FOR THE ENTITY
            *************************************************************************************************/

            //Registering Logging factory
            services.AddSingleton (new LoggerFactory ()
                    .AddConsole ()
                    .AddDebug ())
                .AddLogging ();

            services.AddCors (
                options =>
                options.AddPolicy (
                    "AllowAngularConnectLocal",
                    builder => builder.WithOrigins ("http://localhost:4200")
                    .AllowAnyMethod ()
                    .AllowAnyHeader ()
                )
            ); //Enable cross origin 

            services.AddMvc ();

            //Configuring User Identity and Roles
            services.AddIdentity<ApplicationUser, IdentityRole> ()
                .AddEntityFrameworkStores<ApplicationDbContext> ()
                .AddDefaultTokenProviders ();

            services.Configure<IdentityOptions> (options => {

                //Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                //Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes (30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                //User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie (options => {
                //Cookie settings
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays (150);
                options.LoginPath = "";
                options.LogoutPath = "";
                options.AccessDeniedPath = "";
                options.SlidingExpiration = true;
            });

            // Add Email Service 
            services.AddTransient<IEmailSender, EmailSender> ();

            //Seed the database
            var db = new DbInitializer (services);
            db.Seed ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory LoggerFactory) {

            //Activating logging
            LoggerFactory.AddConsole ();
            LoggerFactory.AddDebug ();

            env.EnvironmentName = EnvironmentName.Development;
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            } else {
                app.UseExceptionHandler ("/Home/Error");
            }

            app.UseStaticFiles ();
            app.UseCors ("AllowAngularConnectLocal");

            // Enables authentication and authorization
            app.UseAuthentication ();

            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "hospitalRoute",
                    template: "{Hospital}/{action}/{id?}");

                routes.MapRoute (
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}